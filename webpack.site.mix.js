let mix = require('laravel-mix');

mix.setPublicPath('Frontend/public');

mix.js('Frontend/resources/assets/js/app.js', 'js')
.sass('Frontend/resources/assets/sass/app.scss', 'css');