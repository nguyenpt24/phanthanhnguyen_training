let mix = require('laravel-mix');

mix.setPublicPath('Backend/public');

mix.js('Backend/resources/assets/js/app.js', 'js')
.sass('Backend/resources/assets/sass/app.scss', 'css');