import MainLayout from '../../layouts/default';

export default {
    data() {
        return {}
    },
    name: 'Images',
    created() {
        this.$emit('update:layout', MainLayout);
    },
}
