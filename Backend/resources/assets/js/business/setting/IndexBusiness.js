import MainLayout from '../../layouts/default';

export default {
    data() {
        return {}
    },
    name: 'Setting',
    created() {
        this.$emit('update:layout', MainLayout);
    },
}
