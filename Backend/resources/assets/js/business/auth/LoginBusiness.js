export default {
    data() {
        return {
            user: {
                mail: '',
                pwd: '',
            },
            errors: {
                mail: '',
                pwd: '',
                error: ''
            },
            redirect: '/home',
            fetchUser: false,
            autofocus: true
        }
    },
    mounted() {
        let input = document.querySelector('[autofocus]');
        if (input) {
            input.focus()
        }
    },
    created() {
        this.$emit('update:layout', 'div');
    },
    methods: {
        redirectHome(){
            this.$router.push( { name: "home.index"} )
        },
        login () {
            this.errors = {
                pwd: '',
                mail: '',
                error: ''
            };
            var app = this;
            this.$auth.login({
                headers: {
                    'Content-Type': 'application/json'
                },
                body: {
                    mail: app.user.mail,
                    pwd: app.user.pwd
                }, // Vue-resource
                data:{
                    mail: app.user.mail,
                    pwd: app.user.pwd
                }, // Axios

                success (res) {
                    if( res.data.status ) {
                        // console.log('success');
                    }
                    else {
                        // console.log(res.data.error.message)
                        this.errors.error = res.data.error.message;
                        this.user.pwd = '';
                    }
                },
                error (err) {
                    if (err.response) {
                        if(err.response.data.errors.pwd) {
                            this.errors.pwd = (err.response.data.errors.pwd[0] )
                        }
                        if(err.response.data.errors.mail) {
                            this.errors.mail = (err.response.data.errors.mail[0])
                        }
                    } else {
                        console.log('Error:', err.message)
                        console.log(err.data)
                        console.log(err)
                    }
                    console.log(err.config)
                },
                fetchUser: app.fetchUser,
                redirect: app.redirect,
            })
        }
    }
}