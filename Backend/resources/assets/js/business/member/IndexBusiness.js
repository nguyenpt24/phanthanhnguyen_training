import MainLayout from '../../layouts/default';

export default {
    data() {
        return {}
    },
    name: 'Member',
    created() {
        this.$emit('update:layout', MainLayout);
    },
}
