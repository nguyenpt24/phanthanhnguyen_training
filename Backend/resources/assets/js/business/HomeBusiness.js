import MainLayout from '../layouts/default';
import {HTTP} from '../config/axios';

export default {
    data() {
        return {
            data: {
                'count_release_number': 0,
                'count_post': 0,
                'count_category': 0,
                'count_user': 0
            }
        }
    },
    name: 'Home',
    created() {
        this.$emit('update:layout', MainLayout);
        this.fetchData();
    },
    methods: {
        /**
         * Get list data
         */
        fetchData() {
            HTTP.get('home')
                .then(response => {
                    this.data = response.data.data;
                })
                .catch(error => {
                    console.log(error);
                })
        },
    }
}
