import MainLayout from '../../layouts/default';

export default {
    data() {
        return {}
    },
    name: 'Category',
    created() {
        this.$emit('update:layout', MainLayout);
    },
}
