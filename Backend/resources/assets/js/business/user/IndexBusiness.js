import MainLayout from '../../layouts/default';

export default {
    data() {
        return {}
    },
    name: 'User',
    created() {
        this.$emit('update:layout', MainLayout);
    },
}
