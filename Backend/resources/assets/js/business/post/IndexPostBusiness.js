import Vue from 'vue'
import MainLayout from '../../layouts/default';
import {HTTP} from '../../config/axios';
import FormSearch from '../../pages/post/FormSearchComponent';
Vue.component('form-search', FormSearch);
Vue.use(require('vue-moment'));

export default {
    data() {
        return {
            paginate: {
                per_page: 20,
                current_page: 1,
                total: 1,
                last_page: 1,
            },
            currentPage: 1,
            totalPage: 0,
            prev_text: '前',
            next_text: '次',
            options: [
                'foo',
                'bar',
                'baz'
            ],
            placeholder: 'すべて',
            items: [],
            item: {
                id: '',
                title: '',
                image_id: '',
                release_date: '',
                status: '',
                user_id: '',
                content: '',
                category_parent: '',
                category_child: '',
                release_number_id: '',
                thumbnail: '',
            },
            index: '',
            id_element: '',
            data_search: '',
            search_reset: false,
            check_request_data: true,
            check_request_paginate: true
        }
    },
    name: 'Post',
    created() {
        this.$emit('update:layout', MainLayout);
        this.getInfo();
        this.initData();
    },
    watch: {
        'currentPage': 'initData',
        'totalPage': 'initData',
        data_search: function () {
            this.getInfo();
            this.initData();
        }
    },
    methods: {
        /**
         * Get list posts
         */
        initData() {
            this.items = [];
            let currentPage = this.currentPage;
            this.paginate.current_page = currentPage ;
            if(!!this.data_search) {
                if(!this.check_request_data) {
                    return false;
                }
                this.check_request_data = false;
                let search = this.data_search;
                HTTP.get('post/'+currentPage,{
                    params: search
                })
                    .then(response => {
                        this.items = response.data.data;
                        this.check_request_data = true;
                    })
                    .catch(e => {
                        console.log(e);
                    })
            } else {
                HTTP.get('post/'+currentPage)
                    .then(response => {
                        this.items = response.data.data;
                    })
                    .catch(e => {
                        console.log(e);
                    })
            }
        },

        /**
         * Get info pagination
         * @return {number} paginate
         */
        getInfo() {
            if(!!this.data_search) {
                if(!this.check_request_paginate) {
                    return false;
                }
                this.check_request_paginate = false;
                let search = this.data_search;
                HTTP.get('post/info-paginate',{
                    params: search
                })
                    .then(response => {
                        this.paginate = response.data.data;
                        this.check_request_paginate = true;
                        return this.paginate;
                    })
                    .then(res => {
                        this.currentPage = res.current_page
                    })
                    .catch(er => {
                        console.log(er);
                    })
            } else {
                HTTP.get('post/info-paginate')
                    .then(response => {
                        this.paginate = response.data.data;
                        return this.paginate;
                    })
                    .then(res => {
                        this.currentPage = res.current_page
                    })
                    .catch(er => {
                        console.log(er);
                    })
            }
        },

        /**
         * Delete release number
         */
        delItem() {
            let post_id = (this.id_element);
            HTTP.delete('post/'+post_id)
                .then(response => {
                    this.cancel('modal-confirm');
                    this.totalPage = this.paginate.total--;
                    this.alertSuccessModal();
                })
                .catch(er => {
                    console.log(er);
                });
        },

        /**
         * Open the modal with refs = modal-confirm
         * @param {Object} item
         */
        showModal(item) {
            this.$refs['modal-confirm'].show();
            this.id_element = item.id;
        },

        /**
         * Open the modal with refs = modal-content
         * @param {Object} item
         * @param {number} index
         */
        showModalContent(item,index) {
            this.$refs['modal-content'].show();
            this.item = item;
            this.index = index;
        },

        /**
         * Close the modal with refs = refs
         * @param {number} refs
         */
        cancel(refs) {
            this.$refs[refs].hide();
        },

        /**
         * Replace the url image when there is an error
         * @param {Object} event
         */
        imgUrlAlt(event) {
            event.target.src = '/images/no-image.jpg';
        },

        /**
         * Alert success Modal
         */
        alertSuccessModal(){
            this.$swal.fire({
                type: 'success',
                title: 'Success',
                showConfirmButton: false,
                timer: 1500
            })
        },

        /**
         * Reset result search
         */
        resetSearch(){
            this.data_search = '';
            this.search_reset = !this.search_reset;
            this.getInfo();
            this.initData();
        },


    }
}
