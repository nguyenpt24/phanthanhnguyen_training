import Vue from 'vue'
import MainLayout from '../../layouts/default';
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
import Datepicker from 'vuejs-datepicker';
import moment from 'moment'
import {HTTP} from '../../config/axios';
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(require('vue-moment'));
Vue.use(VueSweetalert2);
import VueCkeditor from 'vue-ckeditor2';

export default {
    components: {
        vueDropzone: vue2Dropzone,
        Datepicker,
        VueCkeditor
    },
    data() {
        return {
            dropzoneOptions: {
                url: 'http://adminsite.local/api/image',
                thumbnailWidth: 200,
                maxFilesize: 10,
                maxFiles: 1,
                addRemoveLinks: true,
                acceptedFiles: 'image/jpeg,image/png',
            },
            datepicker: {
                format: 'yyyy\年MM\月dd\日\号',
                name: 'name',
                state: new Date()
            },
            item: {
                title: '',
                thumbnail: '',
                category_parent: '',
                category_child: '',
                status: '',
                content: '',
                release_date: '',
                release_number_id: '',
                user_id: ''
            },
            errors: {
                title: '',
                thumbnail: '',
                category_parent: '',
                category_child: '',
                status: '',
                content: '',
                release_date: '',
                other: ''
            },
            status: [
                {
                    value: 1,
                    label: '公開'
                },
                {
                    value: 2,
                    label: '非公開'
                },
                {
                    value: 3,
                    label: '下書き'
                },
            ],
            categories: [],
            created: false,
            renderComponent: true,
            categories_parent: [],
            categories_child: [],
            release_numbers: [],
            config: {
                toolbarGroups: [
                    {name: "document", groups: ["mode", "document", "doctools"]},
                    {name: "clipboard", groups: ["clipboard", "undo"]},
                    {name: "editing", groups: ["find", "selection", "spellchecker", "editing"]},
                    {name: "forms", groups: ["forms"]},
                    {name: "styles", groups: ["styles"]},
                    {name: "colors", groups: ["colors"]},
                    {name: "tools", groups: ["tools"]},
                    {name: "others", groups: ["others"]},
                    {name: "about", groups: ["about"]},
                    "/",
                    {name: "basicstyles", groups: ["basicstyles", "cleanup"]},
                    {name: "paragraph", groups: ["list", "indent", "blocks", "align", "bidi", "paragraph"]},
                    {name: "links", groups: ["links"]},
                    {name: "insert", groups: ["insert"]},
                ],
                height: 456
            },
            user_id: ''
        }
    },
    name: 'CreatePost',
    created() {
        this.$emit('update:layout', MainLayout);
        this.item.release_date = this.datepicker.state;
        this.getCategory();
        this.getReleaseNumber();
        this.fetchUser();
    },
    computed: {},
    watch: {},
    methods: {
        /**
         * Call api create post when submit form
         */
        onSubmit() {
            var item = this.item;
            item.release_date = moment(this.item.release_date).format('YYYY-MM-DD');
            item.user_id = this.user_id;
            HTTP.post('/post', item)
                .then(res => {
                    if (res.data.data) {
                        this.created = true;
                        this.reset();
                        this.alertSuccessModal();
                        console.log(item);
                    } else {
                        this.errors = [];
                        if (res.data.error.message) {
                            this.errors.push(res.data.error.message);
                            this.alertErrorModal()
                        }
                    }
                })
                .catch(error => {
                    this.errors = [];
                    if (error.response.data.errors.title) {
                        this.errors.push(error.response.data.errors.title)
                    }
                    if (error.response.data.errors.thumbnail) {
                        this.errors.push(error.response.data.errors.thumbnail)
                    }
                    if (error.response.data.errors.category_parent) {
                        this.errors.push(error.response.data.errors.category_parent)
                    }
                    if (error.response.data.errors.category_child) {
                        this.errors.push(error.response.data.errors.category_child)
                    }
                    if (error.response.data.errors.status) {
                        this.errors.push(error.response.data.errors.status)
                    }
                    if (error.response.data.errors.content) {
                        this.errors.push(error.response.data.errors.content)
                    }
                    if (error.response.data.errors.release_date) {
                        this.errors.push(error.response.data.errors.release_date)
                    }
                    if (error.response.data.errors.release_number_id) {
                        this.errors.push(error.response.data.errors.release_number_id)
                    }
                    this.alertErrorModal()
                })
        },

        /**
         * The file has been uploaded successfully. Gets the server response as second argument.
         * @param {Object} file
         * @param {Object} response
         */
        vsuccess(file, response) {
            this.item.thumbnail = response.data;
        },

        /**
         * When a file is added to the list
         * @param {Object} file
         */
        vaddfile(file) {
            this.errors = [];
            const b = 1048576;
            if (file.size / b > this.dropzoneOptions.maxFilesize) {
                let error = this.$t('release_number.thumbnail.maxSize');
                this.errors.push(error);
                this.$refs.myVueDropzone.removeFile(file);
            }
            if (this.dropzoneOptions.acceptedFiles.search(file.type) === -1) {
                let error = this.$t('release_number.thumbnail.wrongFormat');
                this.errors.push(error);
                this.$refs.myVueDropzone.removeFile(file);
            }
            if (this.errors.length) {
                this.alertErrorModal();
            }

        },

        /**
         * When a file is added to the list
         * @param {Object} file
         */
        vmaxfile(file) {
            let error = this.$t('release_number.thumbnail.maxFile');
            this.errors = [];
            this.errors.push(error);
            this.$refs.myVueDropzone.removeFile(file);
            this.alertErrorModal();

        },

        /**
         * Called whenever a file is removed from the list.
         * @param {Object} file
         */
        vremoved(file) {
            if (file.status === 'success' && this.created !== true) {
                var id = this.item.thumbnail;
                HTTP.delete('/image/' + id)
                    .then(res => {
                        this.item.thumbnail = '';
                        if (!res.data.data) {
                            console.log(res.data.error.message);
                        }
                    })
                    .catch(err => {
                        console.log(err.errors);
                    })
            }
        },

        /**
         * Reset Dropzone input
         */
        reset() {
            this.item = {
                name: '',
                thumbnail: '',
                description: '',
                price: '',
            }
            this.$refs.myVueDropzone.removeAllFiles();
            this.created = false;
        },

        /**
         * Force rerender DOM
         */
        forceRerender() {
            // Remove my-component from the DOM
            this.renderComponent = false;

            this.$nextTick(() => {
                // Add the component back in
                this.renderComponent = true;
            });
        },

        /**
         * Alert success Modal
         */
        alertSuccessModal() {
            this.$swal.fire({
                type: 'success',
                title: 'Success',
                showConfirmButton: false,
                timer: 1500
            })

        },

        /**
         * Alert error Modal
         */
        alertErrorModal() {
            if (this.errors.length) {
                var error = '<ul class="list-errors">';
                $.each(this.errors, function (index, value) {
                    error += '<li class="error-item"> ' + value + '</li>';
                });
                error += '</ul>';
                this.$swal.fire({
                    type: 'error',
                    title: 'Error',
                    html: error,
                });
            }
        },

        /**
         * Alert confirm Modal
         */
        cancel() {
            this.$swal.fire({
                title: this.$t('alert.confirm.title'),
                text: this.$t('alert.confirm.text'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.value) {
                    this.$router.go(-1);
                }
            })
        },

        /**
         * Get list Categories
         */
        getCategory() {
            HTTP.get('category')
                .then(response => {
                    this.categories = response.data.data;
                    let categories_parent = [];
                    if (this.categories.length) {
                        this.categories.forEach(function (value) {
                            if (value.parent_id === 0) {
                                categories_parent.push(value);
                            }
                        });
                        return categories_parent;
                    }
                })
                .then(res => {
                    this.categories_parent = res;
                })
                .catch(error => {
                    console.log(error);
                })
        },

        /**
         * Get list Categories
         */
        getReleaseNumber() {
            HTTP.get('release_number')
                .then(response => {
                    this.release_numbers = response.data.data;
                })
                .catch(error => {
                    console.log(error);
                })
        },

        /**
         * Select category option
         * @param {number} parent_id
         * @return array
         */
        selectCategoryOptions(parent_id) {
            let items = this.categories;
            let array = [];
            items.forEach(function (value) {
                if (value.parent_id === parent_id) {
                    array.push(value);
                }
            });
            return array.map(g => ({label: g.name, value: g.id}))
        },

        /**
         * Select category
         * @param {number} cat_parent
         */
        selectCategory(cat_parent) {
            this.item.category_child = undefined;
            let categories_child = [];
            if (this.categories.length) {
                this.categories.forEach(function (value) {
                    if (!!cat_parent && value.parent_id === cat_parent.id) {
                        categories_child.push(value);
                    }
                });
            }
            this.categories_child = categories_child;
            if (categories_child.length) {
                this.item.category_child = categories_child[0];
            }
        },

        /**
         * Get user
         * @return {Object} user
         */
        fetchUser() {
            this.$auth.fetch({url: '/api/auth/user', method: 'get'})
                .then(res => {
                    this.user_id = res.data.user.id;
                })
        },
    }
}