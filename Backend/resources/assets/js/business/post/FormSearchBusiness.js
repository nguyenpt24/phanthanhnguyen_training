import Vue from 'vue'
import vSelect from 'vue-select'
import {HTTP} from '../../config/axios';
Vue.component('v-select', vSelect);

export default {
    props:['search_reset'],
    data() {
        return {
            status: [
                {
                    value: 1,
                    label: '公開'
                },
                {
                    value: 2,
                    label: '非公開'
                },
                {
                    value: 3,
                    label: '下書き'
                },
            ],
            placeholder: 'すべて',
            release_numbers: [],
            categories: [],
            categories_parent: [],
            categories_child: [],
            search: {
                release_number: '',
                category_child: '',
                category_parent: '',
                status: '',
                content: '',
            },
            query: [],
            data: ''
        }
    },
    created() {
        this.$router.replace({ query: '' });
        this.getReleaseNumber();
        this.getCategory();
        this.getUrlQuery();
    },
    watch: {
        'query' : 'getUrlQuery',
        'search_reset' : 'resetForm',
    },
    methods: {
        /**
         * Call api create post when submit form
         */
        onSubmit(){
            var search =  {
                category_parent: !!this.search.category_parent  ?  this.search.category_parent.id : null ,
                category_child: !!this.search.category_child  ? this.search.category_child.id : null ,
                status: !!this.search.status ? this.search.status.value : null ,
                content: this.search.content,
                release_number: !!this.search.release_number ? this.search.release_number.id : null ,
            };
            this.$router.replace({ query: search });
            this.getUrlQuery();
            if( !!search.category_parent || !!search.category_child || !!search.status || !!search.content || !!search.release_number ){
                this.$emit('data_search', search );
            } else {
                this.$swal.fire({
                    type: 'warning',
                    title: this.$t("post.search.input_null"),
                    showCloseButton: true,
                })
            }
        },

        /**
         * Get list Categories
         */
        getReleaseNumber(){
            HTTP.get('release_number')
                .then(response => {
                    this.release_numbers = response.data.data;
                    if(this.release_numbers.length){
                        let query =  this.$route.query ;
                        this.release_numbers.forEach(function (value) {
                            if( !!query.release_number && parseInt(query.release_number) === value.id ) {
                                query.release_number = value;
                            }
                        });
                        this.search.release_number = query.release_number;
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        },

        /**
         * Get list Categories
         */
        getCategory(){
            HTTP.get('category')
                .then(response => {
                    this.categories = response.data.data;
                    let categories_parent = [];
                    if(this.categories.length){
                        let query =  this.$route.query ;
                        this.categories.forEach(function (value) {
                            if(value.parent_id === 0 ) {
                                categories_parent.push(value);
                            }
                            if(value.id === 0 ) {
                                categories_parent.push(value);
                            }
                            if(  !!query.category_parent && parseInt(query.category_parent) === value.id ) {
                                query.category_parent = value;
                            }
                            if(  !!query.category_child && parseInt(query.category_child) === value.id ) {
                                query.category_child = value;
                            }
                        });
                        this.search.category_parent = query.category_parent;
                        this.search.category_child = query.category_child;
                        return categories_parent;
                    }
                })
                .then(res=>{
                    this.categories_parent = res;
                })
                .catch(error => {
                    console.log(error);
                })
        },

        /**
         * Select category
         * @param {number} cat_parent
         */
        selectCategory(cat_parent){
            this.search.category_child = null ;
            let categories_child = [];
            if(this.categories.length){
                this.categories.forEach(function (value) {
                    if( !!cat_parent && value.parent_id === cat_parent.id ) {
                        categories_child.push(value);
                    }
                });
            }
            this.categories_child = categories_child;

        },

        /**
         * Get params from url
         * @param {number} cat_parent
         */
        getUrlQuery(){
            let query = this.$route.query;
            this.query = query;
            this.search.content = query.content;
            this.status.forEach(function (element) {
                if(  !!query.status && element.value === parseInt(query.status) ) {
                    query.status = element;
                }
            });
            this.search.status = query.status;
        },

        /**
         * Reset form search
         */
        resetForm(){
            // if(this.search_reset) {
            this.search.release_number = '';
            this.search.category_parent = '';
            this.search.category_child = '';
            this.search.status = '';
            this.search.content = '';
            this.$router.replace({ query: '' });
            // this.search_reset = !this.search_reset;
            // }
        },

    }
}
