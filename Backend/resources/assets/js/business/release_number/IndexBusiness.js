import MainLayout from '../../layouts/default';
import {HTTP} from '../../config/axios';

export default {
    components: {MainLayout},
    data() {
        return {
            paginate: {
                per_page: 20,
                current_page: 1,
                total: 0,
                last_page: 1,
            },
            currentPage: 1,
            totalPage: 0,
            items: [],
            item: {
                id: '',
                description: '',
                name: '',
                price: '',
                thumbnail: '',
            },
            prev_text: '前',
            next_text: '次',
            fields: [
                {
                    key: 'id', label: '#', sortable: true,
                    class: 'col-1 text-center align-middle',
                    tdClass: 'td-stt',
                },
                {
                    key: 'name', label: 'Name',
                    class: 'col-4 align-middle td-name',
                },
                {
                    key: 'thumbnail', label: 'Image',
                    class: 'col-5 align-middle',
                    tdClass: 'td-image text-right',
                    thClass: 'text-center',
                },
                {key: 'actions', label: 'Actions', class: 'col-2 text-center  align-middle', tdClass: ' td-action',}
            ],
            mainProps: {
                class: 'my-1',
            },
            id_element: '',
            i: 0
        }
    },
    name: 'ReleaseNumber',
    created() {
        this.$emit('update:layout', MainLayout);
        this.getInfo();
        this.initData();
    },
    watch: {
        'currentPage': 'initData',
        'totalPage': 'initData',
    },
    methods: {
        /**
         * Open the modal with refs = modal-confirm
         * @param {Object} item
         */
        showModal(item) {
            this.$refs['modal-confirm'].show();
            this.id_element = item.id;
        },

        /**
         * Close the modal with refs = modal-confirm
         *
         */
        cancel() {
            this.$refs['modal-confirm'].hide()
        },

        /**
         * Get list release number
         *
         */
        initData() {
            this.items = [];
            let currentPage = this.currentPage;
            this.paginate.current_page = currentPage;
            HTTP.get('release_number/' + currentPage)
                .then(response => {
                    this.items = response.data.data;
                })
                .catch(e => {
                    console.log(e);
                })
        },

        /**
         * Get info pagination
         * @return {number} paginate
         */
        getInfo() {
            HTTP.get('release_number/info-paginate')
                .then(response => {
                    this.paginate = response.data.data
                    return this.paginate;
                })
                .then(res => {
                    this.currentPage = res.current_page
                })
                .catch(er => {
                    console.log(er);
                })
        },

        /**
         * Delete release number
         */
        delItem() {
            let release_number_id = (this.id_element);
            HTTP.delete('release_number/' + release_number_id)
                .then(response => {
                    this.cancel();
                    this.totalPage = this.paginate.total--;
                })
                .catch(er => {
                    console.log(er);
                });
        },

        /**
         * Get image url with optional parameters
         *
         * @param {number} id
         * @param {number} image_id
         * @param {number} width
         * @param {number} height
         */
        getImage(id, image_id, width, height) {
            if (image_id) {
                HTTP.get('image/' + image_id + '/' + width + 'x' + height)
                    .then(response => {
                        if (response.data.data) {
                            $('#thumbnail-' + id).attr('src', response.data.data);
                        } else {
                            $('#thumbnail-' + id).attr('src', '/images/no-image.jpg');
                        }
                    })
                    .catch(error => {
                        $('#thumbnail-' + id).attr('src', '/images/no-image.jpg');
                    })
            }
        },

        /**
         * Replace the url image when there is an error
         * @param {Object} event
         */
        imgUrlAlt(event) {
            let image = event.target.src.replace('/storage/thumbnail/1200x400', '/storage');
            let element = event.target.id;
            let data = $('#' + element).attr('data-er');
            $('#' + element).attr('data-er', parseInt(++data));
            let id = $('#' + element).attr('data-id');
            let image_id = $('#' + element).attr('data-img-id');
            if (data === 1) {
                event.target.src = image;
                if (image_id) {
                    this.getImage(id, image_id, 1200, 400);
                }
            } else {
                event.target.src = '/images/no-image.jpg';
            }
        }
    }
}
