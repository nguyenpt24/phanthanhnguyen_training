import Vue from "vue"
import MainLayout from "../../layouts/default";
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
import Datepicker from 'vuejs-datepicker';
import moment from 'moment'
import {HTTP} from '../../config/axios';
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(require('vue-moment'));
Vue.use(VueSweetalert2);

export default {
    components: {
        vueDropzone: vue2Dropzone,
        Datepicker
    },
    data() {
        return {
            dropzoneOptions: {
                url: 'http://adminsite.local/api/image',
                thumbnailWidth: 200,
                maxFilesize: 10,
                maxFiles: 1,
                addRemoveLinks: true,
                acceptedFiles: 'image/jpeg,image/png',
            },
            datepicker: {
                format: "yyyy\年MM\月dd\日\号",
                name: 'name',
            },

            item: {
                name: '',
                thumbnail: '',
                description: '',
                price: '',
            },

            errors: {
                release_number_name: '',
                release_number_thumbnail: '',
                release_number_description: '',
                release_number_price: '',
                other: ''
            },
            release_name: '',
            created: false,
            friday: 5,
            renderComponent: true,
        }
    },
    name: 'CreateReleaseNumber',
    created() {
        this.$emit('update:layout', MainLayout);
        this.release_name = this.getNextDayOfWeek(this.friday); //get next Friday (5)
    },
    watch: {
        'release_name': 'selectDate'
    },
    methods: {
        /**
         * Get next day of week
         * @param {number} dayNeed
         * @returns {string}
         */
        getNextDayOfWeek(dayNeed) {
            var today = moment().isoWeekday();
            if (today <= dayNeed) {
                return moment().isoWeekday(dayNeed).format("YYYY-MM-DD HH:mm:ss");
            } else {
                return moment().add(1, 'weeks').isoWeekday(dayNeed).format("YYYY-MM-DD HH:mm:ss");
            }
        },

        /**
         * Call api create release number when submit form
         */
        onSubmit() {
            var name = moment(this.release_name).format("YYYY\年MM\月DD\日\号");
            var item = {
                release_number_name: name,
                release_number_thumbnail: this.item.thumbnail,
                release_number_description: this.item.description,
                release_number_price: this.item.price,
            };
            HTTP.post('/release_number', item)
                .then(res => {
                    if (res.data.data) {
                        this.created = true;
                        this.reset();
                        this.alertSuccessModal();
                    } else {
                        this.errors = [];
                        if (res.data.error.message) {
                            this.errors.push(res.data.error.message);
                            this.alertErrorModal()
                        }
                    }
                })
                .catch(error => {
                    this.errors = [];
                    if (error.response.data.errors.release_number_name) {
                        this.errors.push(error.response.data.errors.release_number_name)
                    }
                    if (error.response.data.errors.release_number_thumbnail) {
                        this.errors.push(error.response.data.errors.release_number_thumbnail)
                    }
                    if (error.response.data.errors.release_number_description) {
                        this.errors.push(error.response.data.errors.release_number_description)
                    }
                    if (error.response.data.errors.release_number_price) {
                        this.errors.push(error.response.data.errors.release_number_price)
                    }
                    this.alertErrorModal()
                })
        },

        /**
         * Set release_name by datepicker input
         */
        selectDate() {
            var type = typeof (this.release_name);
            if (type === 'object') {
                var state = '';
                var dayCheck = moment(this.release_name).isoWeekday();
                if (dayCheck <= this.friday) {
                    state = moment(this.release_name).isoWeekday(this.friday).format("YYYY-MM-DD HH:mm:ss");
                } else {
                    state = moment(this.release_name).add(1, 'weeks').isoWeekday(this.friday).format("YYYY-MM-DD HH:mm:ss");
                }
                this.release_name = state;
            }
            this.forceRerender();

        },

        /**
         * The file has been uploaded successfully. Gets the server response as second argument.
         * @param {Object} file
         * @param {Object} response
         */
        vsuccess(file, response) {
            this.item.thumbnail = response.data;
        },

        /**
         * When a file is added to the list
         * @param {Object} file
         */
        vaddfile(file) {
            this.errors = [];
            const b = 1048576;
            if (file.size / b > this.dropzoneOptions.maxFilesize) {
                let error = this.$t("release_number.thumbnail.maxSize");
                this.errors.push(error);
                this.$refs.myVueDropzone.removeFile(file);
            }

            if (this.dropzoneOptions.acceptedFiles.search(file.type) === -1) {
                let error = this.$t("release_number.thumbnail.wrongFormat");
                this.errors.push(error);
                this.$refs.myVueDropzone.removeFile(file);
            }

            if (this.errors.length) {
                this.alertErrorModal();
            }

        },

        /**
         * Called when the number of files accepted reaches the maxFiles limit.
         * @param {Object} file
         */
        vmaxfile(file) {
            let error = this.$t("release_number.thumbnail.maxFile");
            this.errors = [];
            this.errors.push(error);
            this.$refs.myVueDropzone.removeFile(file);
            this.alertErrorModal();

        },

        /**
         * Called whenever a file is removed from the list.
         * @param {Object} file
         */
        vremoved(file) {
            if (file.status === 'success' && this.created !== true) {
                var id = this.item.thumbnail;
                HTTP.delete('/image/' + id)
                    .then(res => {
                        this.item.thumbnail = '';
                        if (!res.data.data) {
                            console.log(res.data.error.message);
                        }
                    })
                    .catch(err => {
                        console.log(err.errors);
                    })
            }
        },

        /**
         * Reset dropzone input .
         */
        reset() {
            this.release_name = this.getNextDayOfWeek(this.friday); //get next Friday (5)
            this.item = {
                name: this.release_name,
                thumbnail: '',
                description: '',
                price: '',
            }
            this.$refs.myVueDropzone.removeAllFiles();
            this.created = false;
        },

        /**
         * Force rerender DOM
         */
        forceRerender() {
            // Remove my-component from the DOM
            this.renderComponent = false;

            this.$nextTick(() => {
                // Add the component back in
                this.renderComponent = true;
            });
        },

        /**
         * Alert success Modal
         */
        alertSuccessModal() {
            this.$swal.fire({
                type: 'success',
                title: 'Success',
                showConfirmButton: false,
                timer: 1500
            })

        },

        /**
         * Alert error Modal
         */
        alertErrorModal() {
            if (this.errors.length) {
                var error = '<ul class="list-errors">';
                $.each(this.errors, function (index, value) {
                    error += '<li class="error-item"> ' + value + '</li>';
                });
                error += '</ul>';
                this.$swal.fire({
                    type: 'error',
                    title: 'Error',
                    html: error,
                });
            }
        },

        /**
         * Alert confirm Modal
         */
        cancel() {
            this.$swal.fire({
                title: this.$t("alert.confirm.title"),
                text: this.$t("alert.confirm.text"),
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.value) {
                    this.$router.go(-1);
                }
            })
        },
    }
}
