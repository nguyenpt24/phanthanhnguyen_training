import Vue from 'vue'
import VueI18n from 'vue-i18n'
import jpMessage from './jp.json'


Vue.use(VueI18n)

const messages = {
  jp: jpMessage,
}

const i18n = new VueI18n({
  locale: 'jp', // set locale
  messages,
  fallbackLocale: 'jp',
})

export default i18n