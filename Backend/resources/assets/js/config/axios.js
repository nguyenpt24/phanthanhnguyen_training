//file http-common.js
import axios from 'axios';
const token = localStorage.getItem('auth-token');
export const HTTP = axios.create({
    baseURL: 'http://adminsite.local/api/',
    headers: {
        Authorization: 'Bearer '+token
    }
})
