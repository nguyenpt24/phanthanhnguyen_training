/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue'
import VueAuth from '@websanova/vue-auth';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import router from './router';
import App from './pages/App';
import auth from './config/auth';
import i18n from './src/lang/i18n'

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios);
Vue.router = router;
Vue.use(VueRouter);
Vue.use(VueAuth, auth);

const app = new Vue({
    el: '#app',
    router,
    i18n,
    components: {App},
});
