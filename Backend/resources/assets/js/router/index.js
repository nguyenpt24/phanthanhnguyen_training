import Vue from 'vue'
import Router from 'vue-router'
import LoginComponent from '../pages/auth/LoginComponent'
import HomeComponent from '../pages/HomeComponent';
import ReleaseComponent from '../pages/release_number/IndexComponent';
import AddReleaseComponent from '../pages/release_number/AddComponent';
import EditReleaseComponent from '../pages/release_number/EditComponent';
import IndexPostComponent from '../pages/post/IndexPostComponent';
import AddPostComponent from '../pages/post/AddPostComponent';
import EditPostComponent from '../pages/post/EditPostComponent';

import CategoryComponent from '../pages/category/IndexComponent';
import SettingComponent from '../pages/setting/IndexComponent';
import MemberComponent from '../pages/member/IndexComponent';
import UserComponent from '../pages/user/IndexComponent';
import ImageComponent from '../pages/image/IndexComponent';

Vue.use(Router);

var routers = [];
routers = [
    {
        path: '/login',
        name: 'auth.login',
        component: LoginComponent,
        meta: {
            auth: false,
        }
    },
    {
        path: '/home',
        name: 'admin.home',
        component: HomeComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/release-number',
        name: 'admin.release_number.index',
        component: ReleaseComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','editor']
        }
    },
    {
        path: '/release-number/add',
        name: 'admin.release_number.add',
        component: AddReleaseComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','editor']
        }
    },
    {
        path: '/release-number/edit/:id',
        name: 'admin.release_number.edit',
        component: EditReleaseComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','editor']
        }
    },
    {
        path: '/category',
        name: 'admin.category.index',
        component: CategoryComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','editor']
        }
    },
    {
        path: '/user',
        name: 'admin.user.index',
        component: UserComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin']
        }
    },
    {
        path: '/post',
        name: 'admin.post.index',
        component: IndexPostComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/post/{search}',
        name: 'admin.post.search',
        component: IndexPostComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/post/add',
        name: 'admin.post.add',
        component: AddPostComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/post/edit/:id',
        name: 'admin.post.edit',
        component: EditPostComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/image',
        name: 'admin.image.index',
        component: ImageComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/member',
        name: 'admin.member.index',
        component: MemberComponent,
        linkActiveClass: "is_active",
        meta: {
            auth: true,
            roles: ['admin','superadmin','contributor','editor']
        }
    },
    {
        path: '/setting',
        name: 'admin.setting.index',
        component: SettingComponent,
        meta: {
            auth: true,
            roles: ['admin','superadmin']
        }
    },

    {
        // not found handler
        path: '*',
        redirect: '/login'
    }
];

var router = new Router({
    mode: 'history',
    routes: routers,
});

Vue.router = router;

export default router;
