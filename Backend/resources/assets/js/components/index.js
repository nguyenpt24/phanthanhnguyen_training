import Vue from 'vue'
import LoginComponent from '../pages/auth/LoginComponent'
import HomeComponent from "../pages/HomeComponent";
import LayoutComponent from "../layouts/default";
import HeaderComponent from './admin/HeaderComponent'
import FooterComponent from './admin/FooterComponent'
import LeftbarComponent from "./admin/LeftbarComponent";

[
    LoginComponent,
    HomeComponent,
    LayoutComponent,
    HeaderComponent,
    LeftbarComponent,
    FooterComponent,

].forEach(Component => {
    Vue.component(Component.name, Component)
})
