<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => [
        'user_lock' => 'パスワードに5回連続で誤りがあったため、アカウントをロックしました。管理者に連絡を行ってください。',
        'login_fail' => 'ユーザー情報が正しくない。',
        'error_other' => '情報処理の中に、エーラが発生しました。',
        'unauthorized' => 'このリソースにアクセスする権限がありません ',
    ],

    'logout' => [
        'success' => 'ユーザーがログアウトしました',
        'error_other' => '情報処理の中に、エラーが発生しました。',
    ]
    
];
