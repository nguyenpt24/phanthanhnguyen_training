<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'messages' => [
        'init_data_error'   => '発売号のデータの取得が失敗しました。',
        'del_error'         => 'この発売号は削除ができません。',
        'save_error'        => 'この発売号は保存ができません。',
        'image_large_size'  => '発売号画像が10mb以下のみ有効です。',
        'image_format'      => '発売号画像は「png」「jpg」のみ有効です。',
        'other_error'       => '情報処理の中に、エラーが発生しました。',

    ]
    
];
