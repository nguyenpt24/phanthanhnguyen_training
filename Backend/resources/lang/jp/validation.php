<?php

return [
    'mail' => [
        'required' => 'メールを入力してください。',
        'max'      => 'メールは6文字から:max文字まで入力してください。',
    ],

    'password' => [
        'required' => 'パスワードを入力してください。',
        'max' => 'パスワードは6文字から:max文字まで入力してください。',
    ],

    'release_number' => [
        'name' => [
            'required' => '発売号を入力してください。',
            'max' => '発売号が:max文字以下み有効です。',
            'unique' => 'リリース番号の名前はすでに使用されています。',
        ],
        'thumbnail' => [
            'required' => '発売号画像を入力してください。',
        ],
        'description' => [
            'max' => '形容が:max文字以下み有効です。',
        ],
    ],

    'post' => [
        'title' => [
            'required' => 'タイトルを入力してください。',
            'maxmin' => 'タイトルは10文字から300文字まで入力してください。',
        ],
        'release_date' => [
            'required' => '公開日を入力してください。',
            'max' => '公開日は:max文字以下み有効です。',
        ],
        'category_parent' => [
            'required' => '親カテゴリを入力してください。',
        ],
        'status' => [
            'required' => 'ステータスを入力してください。',
        ],
        'category_child' => [
            'required' => '子カテゴリを入力してください。',
        ],
        'release_number_id' => [
            'required' => '発売号を入力してください。',
        ],
        'content' => [
            'required' => '記事内容を入力してください。',
        ],
        'thumbnail' => [
            'required' => '画像を入力してください。',
        ],
    ]
];
