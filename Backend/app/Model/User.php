<?php

namespace Backend;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mail', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Method get JWT Identifier
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Method get JWT Custom Claims
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the role that owns the user.
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('Backend\Model\Role', 'role_id');
    }

    /**
     * Get the posts for the user.
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('Backend\Model\Post');
    }
}
