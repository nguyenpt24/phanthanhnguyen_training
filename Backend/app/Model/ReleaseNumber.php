<?php

namespace Backend\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ReleaseNumber
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumber extends Model
{
    /**
     * @var string
     */
    protected $table = 'release_numbers';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image_id', 'description', 'price',
    ];

    /**
     * Get the posts for the release number.
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('Backend\Model\Post');
    }

    /**
     * Get the image that owns the release number.
     * @return BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('Backend\Model\Image');
    }

    /**
     * This is a recommended way to declare event handlers
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($release_number) { // before delete() method call this
            $release_number->image()->delete();
            $release_number->posts()->delete();
            // do the rest of the cleanup...
        });
    }
}
