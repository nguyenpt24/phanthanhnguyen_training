<?php

namespace Backend\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class Role extends Model
{
    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the users for the role.
     */
    public function users()
    {
        return $this->hasMany('Backend\User');
    }
}
