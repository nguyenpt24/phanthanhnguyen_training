<?php

namespace Backend\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Category
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'parent_id'
    ];

    /**
     * Get the posts for the image.
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('Backend\Model\Post');
    }

    /**
     * Get the release_numbers for the image.
     * @return HasMany
     */
    public function release_numbers()
    {
        return $this->hasMany('Backend\Model\ReleaseNumber', 'image_id');
    }

}
