<?php

namespace Backend\Http\Controllers;

use Backend\Repositories\Eloquent\CategoryRepository;
use Backend\Repositories\Eloquent\PostRepository;
use Backend\Repositories\Eloquent\ReleaseNumberRepository;
use Backend\Repositories\Eloquent\UserRepository;

/**
 * Class HomeController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-20
 */
class HomeController extends Controller
{
    /**
     * @var ReleaseNumberRepository
     */
    protected $releaseNumber;

    /**
     * @var PostRepository
     */
    protected $post;

    /**
     * @var CategoryRepository
     */
    protected $category;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @param ReleaseNumberRepository $releaseNumber
     * @param PostRepository $post
     * @param CategoryRepository $category
     * @param UserRepository $user
     */
    public function __construct(ReleaseNumberRepository $releaseNumber, PostRepository $post, CategoryRepository $category, UserRepository $user)
    {
        $this->releaseNumber = $releaseNumber;
        $this->post = $post;
        $this->category = $category;
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     * @return JsonResponseAlias
     */
    public function index()
    {
        $count_release_number = $this->releaseNumber->countData();
        $count_post = $this->post->countData();
        $count_category = $this->category->countData();
        $count_user = $this->user->countMember();
        $data = array(
            'count_release_number' => $count_release_number,
            'count_post' => $count_post,
            'count_category' => $count_category,
            'count_user' => $count_user,
        );
        return $this->response($data);
    }

    /**
     * Custom Response
     * @param null $data
     * @param int $code
     * @param string $message
     * @return JsonResponseAlias
     */
    public function response($data = null, $code = 200, $message = null)
    {
        $status = $code !== 200;
        $error = array();
        $error['status'] = $status;
        if ($status) {
            $error['code'] = $code;
            $error['message'] = $message;
        }
        return response()->json([
            'data' => $data,
            'error' => $error
        ]);
    }
}
