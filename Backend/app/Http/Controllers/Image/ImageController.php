<?php

namespace Backend\Http\Controllers\Image;

use Backend\Repositories\Eloquent\ImageRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Backend\Http\Controllers\Controller;

/**
 * Class ImageController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ImageController extends Controller
{
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * ImageController constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            if ($request->file('file')) {
                $image = $request->file('file');
                $real_name = preg_replace('/\..+$/', '', $image->getClientOriginalName());
                $ext = $image->getClientOriginalExtension();
                $name = 'media' . date('Y-m-d') . '_' . time() . '_' . uniqid() . '.' . $ext;
                $image->move(storage_path() . '/app/public/images/', $name);
                $url = '/storage/images/' . $name;
                $Image = array(
                    'alt' => $real_name,
                    'url' => $url
                );
                $image = $this->imageRepository->create($Image);
                return response()->json([
                    'data' => $image->id,
                    'error' => [
                        'status' => false,
                    ]
                ], 200);
            } else {
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => __('image.messages.upload_error'),
                    ]
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => __('image.messages.other_error'),
                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $image = $this->imageRepository->find($id);
            if ($this->imageRepository->delete($id)) {
                $this->imageRepository->deleteImage($image);
                return response()->json([
                    'data' => true,
                    'error' => [
                        'status' => false,
                    ]
                ]);
            } else {
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'code' => 400,
                        'message' => __('image.messages.del_error'),
                    ]
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => __('image.messages.other_error'),
                ]
            ]);
        }
    }

    /**
     * Get a thumbnail of specified size
     *
     * @param int $id
     * @param int $width
     * @param int $height
     * @return JsonResponse
     */
    public function getImage($id, $width, $height)
    {
        try {
            $image = $this->imageRepository->find($id);
            $url = $image->url;
            $tmp = explode('/storage/images/', $url);
            $file_name = $tmp[1];
            $url_image = $this->imageRepository->createThumbnail(storage_path() . '/app/public/images', $file_name,
                $width, $height);
            return response()->json([
                'data' => $url_image,
                'error' => [
                    'status' => false,
                ]
            ]);
        } catch (Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => __('image.messages.other_error'),
                ]
            ]);
        }
    }
}
