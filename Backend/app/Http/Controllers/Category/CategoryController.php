<?php

namespace Backend\Http\Controllers\Category;

use Backend\Repositories\Eloquent\CategoryRepository;
use Backend\Http\Controllers\Controller;
use Backend\Transformers\CategoryTransformer;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * Class CategoryController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $category;

    /**
     * @var Manager
     */
    private $fractal;

    /**
     * @var CategoryTransformer
     */
    private $categoryTransformer;

    /**
     * CategoryController constructor.
     * @param Manager $fractal
     * @param CategoryTransformer $categoryTransformer
     * @param CategoryRepository $category
     */
    public function __construct(
        Manager $fractal,
        CategoryTransformer $categoryTransformer,
        CategoryRepository $category
    )
    {
        $this->fractal = $fractal;
        $this->categoryTransformer = $categoryTransformer;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $list = $this->category->getAll();
        $categories = new Collection($list,
            $this->categoryTransformer); // Create a resource collection transformer
        $categories = $this->fractal->createData($categories); // Transform data
        return response()->json([
            'data' => $categories->toArray()['data'],
            'error' => [
                'status' => false,
            ]
        ]);
    }
}
