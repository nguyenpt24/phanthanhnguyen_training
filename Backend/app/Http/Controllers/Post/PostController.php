<?php

namespace Backend\Http\Controllers\Post;

use Backend\Http\Requests\Post\PostRequest;
use Backend\Repositories\Eloquent\ImageRepository;
use Exception;
use Backend\Repositories\Eloquent\PostRepository;
use Backend\Http\Controllers\Controller;
use Backend\Transformers\PostTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse as JsonResponseAlias;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * PostController.php
 * Class PostController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostController extends Controller
{
    /**
     * @var PostRepository
     */
    protected $post;

    /**
     * @var Manager
     */
    private $fractal;

    /**
     * @var PostTransformer
     */
    private $postTransformer;

    /**
     * PostController constructor.
     * @param Manager $fractal
     * @param PostTransformer $postTransformer
     * @param PostRepository $post
     */
    public function __construct(
        Manager $fractal,
        PostTransformer $postTransformer,
        PostRepository $post
    )
    {
        $this->fractal = $fractal;
        $this->postTransformer = $postTransformer;
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponseAlias
     */
    public function index()
    {
        $list = $this->post->getAll();
        $posts = new Collection($list, $this->postTransformer); // Create a resource collection transformer
        $posts = $this->fractal->createData($posts); // Transform data
        $data = $posts->toArray()['data'];
        return $this->response($data);
    }

    /**
     * Get list posts according to certain page numbers
     *
     * @param Request $request
     * @param int $page
     * @return JsonResponseAlias
     */
    public function paginate(Request $request, $page)
    {
        try {
            $arSearch = $request->all();
            if (empty($arSearch)) {
                $list = $this->post->paginate($page);
            } else {
                $list = $this->post->search($arSearch, $page);
            }
            $posts = new Collection($list, $this->postTransformer); // Create a resource collection transformer
            $posts = $this->fractal->createData($posts); // Transform data
            $data = $posts->toArray()['data'];
            return $this->response($data);
        } catch (Exception $e) {
            return $this->response(false, 500, __('post.messages.init_data_error'));
        }
    }

    /**
     * Get info Paginate
     * @param Request $request
     * @return JsonResponseAlias
     */
    public function infoPaginate(Request $request)
    {
        try {
            $arSearch = $request->all() ?? null;
            if (empty($arSearch)) {
                $list = $this->post->paginate(1);
            } else {
                $list = $this->post->search($arSearch, 1);
            }
            $data = [
                'per_page' => $list->perPage(),
                'current_page' => $list->currentPage(),
                'total' => $list->total(),
                'last_page' => $list->lastPage(),
            ];
            return $this->response($data);
        } catch (Exception $e) {
            return $this->response(false, 500, __('post.messages.init_data_error'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponseAlias
     */
    public function destroy($id)
    {
        try {
            $imageRepository = new ImageRepository();
            $post = $this->post->find($id);
            $image_id = $post->image_id ?? null;
            $image = $imageRepository->find($image_id);
            if ($this->post->delete($id)) {
                $imageRepository->deleteImage($image);
                return $this->response(true);
            } else {
                return $this->response(false, 400, __('post.messages.del_error'));
            }
        } catch (Exception $e) {
            return $this->response(false, 500, __('post.messages.other_error'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @return JsonResponseAlias
     */
    public function store(PostRequest $request)
    {
        try {
            $arItem = [
                'title' => $request->title,
                'image_id' => (int)$request->thumbnail,
                'category_parent' => $request->category_parent['id'],
                'category_child' => $request->category_child['id'],
                'status' => (int)$request->status['value'],
                'content' => $request->content,
                'release_date' => $request->release_date,
                'release_number_id' => (int)$request->release_number_id['id'],
                'user_id' => (int)$request->user_id,
            ];
            $post = $this->post->create($arItem);
            if ($post) {
                return $this->response(true);
            } else {
                return $this->response(false, 400, __('post.messages.save_error'));
            }
        } catch (Exception $e) {
            return $this->response(false, 500, __('post.messages.other_error'));
        }
    }

    /**
     * Display 1 resource according to the input parameter
     *
     * @param int $id
     * @return JsonResponseAlias
     */
    public function show($id)
    {
        try {
            if ($post = $this->post->find($id)) {
                $data = $this->postTransformer->transformShow($post);
                return $this->response($data);
            }
            return $this->response(false, 400, __('post.messages.init_data_error'));
        } catch (Exception $e) {
            return $this->response(false, 500, __('post.messages.init_data_error'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest $request
     * @return JsonResponseAlias
     */
    public function update(PostRequest $request)
    {
        try {
            $id = trim($request->id);
            $old_post = $this->post->find($id);
            $old_image_id = $old_post->image_id ?? null;
            $arItem = [
                'title' => $request->title,
                'image_id' => (int)$request->thumbnail,
                'category_parent' => $request->category_parent['id'],
                'category_child' => $request->category_child['id'],
                'status' => (int)$request->status['value'],
                'content' => $request->content,
                'release_date' => $request->release_date,
                'release_number_id' => (int)$request->release_number_id['id'],
                'user_id' => (int)$request->user_id,
            ];
            $post = $this->post->update($id, $arItem);
            if ($post) {
                if ($old_image_id !== $arItem['image_id']) {
                    $imageRepository = new ImageRepository();
                    $image = $imageRepository->find($old_image_id) ?? null;
                    $imageRepository->deleteImage($image);
                }
                return $this->response(true);
            } else {
                return $this->response(false, 400, __('post.messages.save_error'));
            }
        } catch (Exception $e) {
            return $this->response(false, 400, __('post.messages.other_error'));
        }
    }

    /**
     * Custom Response
     * @param null $data
     * @param int $code
     * @param string $message
     * @return JsonResponseAlias
     */
    public function response($data = null, $code = 200, $message = null)
    {
        $status = $code !== 200;
        $error = array();
        $error['status'] = $status;
        if ($status) {
            $error['code'] = $code;
            $error['message'] = $message;
        }
        return response()->json([
            'data' => $data,
            'error' => $error
        ]);
    }
}
