<?php

namespace Backend\Http\Controllers\Auth;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Backend\Http\Controllers\Controller;
use Backend\Http\Requests\Auth\LoginRequest;
use Backend\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response as ResponseAlias;
use Tymon\JWTAuth\Facades\JWTAuth as JWA;
use Tymon\JWTAuth\JWTAuth;
use Auth;
use Exception;

/**
 * Class AuthController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @var JWTAuth
     */
    protected $jwt;

    /**
     * @var int
     */
    protected $maxLoginAttempts = 5;

    /**
     * AuthController constructor.
     * @param JWTAuth $jwt
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Method attempts to log a user in and generates an authorization token
     * @param LoginRequest $request
     * @return ResponseFactory|JsonResponse|ResponseAlias
     */
    public function login(LoginRequest $request)
    {
        try {
            $credentials = array(
                'mail' => $request->mail,
                'password' => $request->pwd
            );
            $user = User::where('mail', $request->mail)->first();
            if (empty($user->status) && !empty($user)) {
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'message' => __('auth.login.user_lock'),
                    ]
                ]);
            }

            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                if (!empty($user)) {
                    $user->status = 0;
                    $user->save();
                }
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'message' => __('auth.login.user_lock'),
                    ]
                ]);
            }

            if (!$token = $this->jwt->attempt($credentials)) {
                $this->incrementLoginAttempts($request);
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'message' => __('auth.login.login_fail'),

                    ]
                ]);
            }

            return response([
                'status' => 'success'
            ])
                ->header('Authorization', $token);
        } catch (Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => __('auth.login.error_other'),
                ]
            ]);
        }
    }

    /**
     * Method returns the user object based on the authorization token that is passed.
     * @return JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWA::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
        $ar_user = compact('user')['user'];
        $user = array(
            'id' => $ar_user['id'],
            'name' => $ar_user['name'],
            'mail' => $ar_user['mail'],
            'role' => $ar_user['role']
        );
        return response()->json(compact('user'));
    }

    /**
     * Method will clear the authentication information in the user's session
     * @return JsonResponse
     */
    public function logout()
    {
        try {
            JWA::invalidate();
            return response()->json([
                'statusCode' => 200,
                'statusMessage' => 'success',
                'message' => __('auth.logout.success'),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'code' => 400,
                    'message' => __('auth.logout.error_other'),
                ]
            ]);
        }
    }

    /**
     * Method creates a user
     * @param Request $request
     * @return ResponseFactory|ResponseAlias
     */
    public function register(Request $request)
    {
        $user = new User;
        $user->mail = $request->mail;
        $user->name = $request->name;
        $user->status = 1;
        $user->roles_id = $request->roles_id;
        $user->password = bcrypt($request->pwd);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }
}

