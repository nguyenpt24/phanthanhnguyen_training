<?php

namespace Backend\Http\Controllers\Auth;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Backend\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Tymon\JWTAuth\JWTAuth;
use Backend\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @var JWTAuth
     */
    protected $jwt;

    /**
     * @var int
     */
    protected $maxLoginAttempts = 5;

    /**
     * LoginController constructor.
     * @param JWTAuth $jwt
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Method attempts to log a user in and generates an authorization token
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = array(
            'mail' => $request->mail,
            'password' => $request->pwd
        );
        $user = User::where('mail', $request->mail)->first();
        if ($user->is_lock) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => 'アカウントをロックしました。管理者に連絡を行ってください。'
                ]
            ]);
        }
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            if ($user) {
                $user->is_lock = 1;
                $user->save();
            }
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => 'パスワードに5回連続で誤りがあったため、アカウントをロックしました。管理者に連絡を行ってください。'
                ]
            ]);
        }
        try {
            if (!$token = $this->jwt->attempt($credentials)) {
                $this->incrementLoginAttempts($request);
                return response()->json([
                    'data' => false,
                    'error' => [
                        'status' => true,
                        'message' => 'ユーザー情報が正しくない。'

                    ]
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => '情報処理の中に、エーラが発生しました。'
                ]
            ]);
        }
        return response()->json([
            'data' => compact('token'),
            'error' => [
                'status' => false,

            ]
        ]);
    }
}
