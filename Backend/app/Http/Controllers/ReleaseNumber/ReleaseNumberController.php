<?php

namespace Backend\Http\Controllers\ReleaseNumber;

use Backend\Repositories\Eloquent\ImageRepository;
use Exception;
use Backend\Http\Requests\ReleaseNumber\ReleaseNumberRequest;
use Backend\Repositories\Eloquent\ReleaseNumberRepository;
use Backend\Http\Controllers\Controller;
use Backend\Transformers\ReleaseNumberTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * Class ReleaseNumberController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumberController extends Controller
{
    /**
     * @var ReleaseNumberRepository
     */
    protected $releaseNumber;

    /**
     * @var Manager
     */
    private $fractal;

    /**
     * @var ReleaseNumberTransformer
     */
    private $releaseNumberTransformer;

    /**
     * ReleaseNumberController constructor.
     * @param Manager $fractal
     * @param ReleaseNumberTransformer $releaseNumberTransformer
     * @param ReleaseNumberRepository $releaseNumber
     */
    public function __construct(
        Manager $fractal,
        ReleaseNumberTransformer $releaseNumberTransformer,
        ReleaseNumberRepository $releaseNumber
    )
    {
        $this->fractal = $fractal;
        $this->releaseNumberTransformer = $releaseNumberTransformer;
        $this->releaseNumber = $releaseNumber;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponseAlias
     */
    public function index()
    {
        $list = $this->releaseNumber->getAll();
        $release_numbers = new Collection($list,
            $this->releaseNumberTransformer); // Create a resource collection transformer
        $release_numbers = $this->fractal->createData($release_numbers); // Transform data
        $data = $release_numbers->toArray()['data'];
        return $this->response($data);
    }

    /**
     * Get list Release number according to certain page numbers
     *
     * @param int $page
     * @return JsonResponseAlias
     */
    public function paginate($page)
    {
        try {
            $list = $this->releaseNumber->paginate($page);
            $release_numbers = new Collection($list,
                $this->releaseNumberTransformer); // Create a resource collection transformer
            $release_numbers = $this->fractal->createData($release_numbers); // Transform data
            $data = $release_numbers->toArray()['data'];
            return $this->response($data);
        } catch (Exception $e) {
            return $this->response(false, 500, __('release_number.messages.init_data_error'));
        }
    }

    /**
     * Get info Paginate
     *
     * @return JsonResponseAlias
     */
    public function infoPaginate()
    {
        try {
            $list = $this->releaseNumber->paginate(1);
            $data = [
                'per_page' => $list->perPage(),
                'current_page' => $list->currentPage(),
                'total' => $list->total(),
                'last_page' => $list->lastPage(),
            ];
            return $this->response($data);
        } catch (Exception $e) {
            return $this->response(false, 500, __('release_number.messages.init_data_error'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReleaseNumberRequest $request
     * @return JsonResponseAlias
     */
    public function store(ReleaseNumberRequest $request)
    {
        try {
            $arReleaseNumber = array(
                'name' => trim($request->release_number_name),
                'image_id' => trim($request->release_number_thumbnail),
                'description' => trim($request->release_number_description),
                'price' => (int)trim($request->release_number_price),
            );
            $releaseNumber = $this->releaseNumber->create($arReleaseNumber);
            if ($releaseNumber) {
                return $this->response(true);
            } else {
                return $this->response(false, 400, __('release_number.messages.save_error'));
            }
        } catch (Exception $e) {
            return $this->response(false, 500, __('release_number.messages.other_error'));
        }
    }

    /**
     * Display 1 resource according to the input parameter
     *
     * @param int $id
     * @return JsonResponseAlias
     */
    public function show($id)
    {
        try {
            $release_number = $this->releaseNumber->find($id);
            $data = $this->releaseNumberTransformer->transform($release_number);
            return $this->response($data);
        } catch (Exception $e) {
            return $this->response(false, 500, __('release_number.messages.init_data_error'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReleaseNumberRequest $request
     * @return JsonResponseAlias
     */
    public function update(ReleaseNumberRequest $request)
    {
        try {
            $id = trim($request->id);
            $old_rl = $this->releaseNumber->find($id);
            $old_image_id = $old_rl->image_id ?? null;
            $arReleaseNumber = array(
                'name' => trim($request->release_number_name),
                'image_id' => trim($request->release_number_thumbnail),
                'description' => trim($request->release_number_description),
                'price' => trim($request->release_number_price),
            );
            $releaseNumber = $this->releaseNumber->update($id, $arReleaseNumber);
            if ($releaseNumber) {
                if ($old_image_id !== $arReleaseNumber['image_id']) {
                    $imageRepository = new ImageRepository();
                    $image = $imageRepository->find($old_image_id) ?? null;
                    $imageRepository->deleteImage($image);
                }
                return $this->response(true);
            } else {
                return $this->response(false, 400, __('release_number.messages.save_error'));
            }
        } catch (Exception $e) {
            return $this->response(false, 400, __('release_number.messages.other_error'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponseAlias
     */
    public function destroy($id)
    {
        try {
            $imageRepository = new ImageRepository();
            $release = $this->releaseNumber->find($id);
            $image_id = $release->image_id ?? null;
            $image = $imageRepository->find($image_id) ?? null;
            if ($this->releaseNumber->delete($id)) {
                $res = $imageRepository->deleteImage($image);
                return $this->response($res);
            } else {
                return $this->response(false, 400, __('release_number.messages.del_error'));
            }
        } catch (Exception $e) {
            return $this->response(false, 500, __('release_number.messages.other_error'));
        }
    }

    /**
     * @return JsonResponseAlias
     */
    public function isActive()
    {
        $list = $this->releaseNumber->getItemsActive();
        $release_numbers = new Collection($list,
            $this->releaseNumberTransformer); // Create a resource collection transformer
        $release_numbers = $this->fractal->createData($release_numbers); // Transform data
        $data = $release_numbers->toArray()['data'];
        return $this->response($data);
    }

    /**
     * Custom Response
     * @param null $data
     * @param int $code
     * @param string $message
     * @return JsonResponseAlias
     */
    public function response($data = null, $code = 200, $message = null)
    {
        $status = $code !== 200;
        $error = array();
        $error['status'] = $status;
        if ($status) {
            $error['code'] = $code;
            $error['message'] = $message;
        }
        return response()->json([
            'data' => $data,
            'error' => $error
        ]);
    }
}
