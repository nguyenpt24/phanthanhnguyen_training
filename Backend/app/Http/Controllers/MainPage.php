<?php

namespace Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MainPage extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        return view('welcome', ['title' => 'Backend Laravel']);
    }
}
