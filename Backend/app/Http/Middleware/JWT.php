<?php

namespace Backend\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Illuminate\Http\Request;

class JWT extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     * @param Request $request
     * @param Closure $next
     * @param string $roles
     * @return JsonResponse|mixed
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException){
                return response()->json(['status' => 'Token is Invalid']);
            }else if ($e instanceof TokenExpiredException){
                return response()->json(['status' => 'Token is Expired']);
            }else{
                return response()->json(['status' => 'Authorization Token not found '.$e->getMessage() ]);
            }
        }
        if ($user && in_array($user->role->name, explode('|', $roles))) {
            return $next($request);
        }
        return $this->unauthorized();
    }

    /**
     * Method returns the response if unauthorized
     * @param null $message
     * @return JsonResponse
     */
    private function unauthorized($message = null)
    {
        return response()->json([
            'data' => false,
            'error' => [
                'status' => true,
                'code' => 401,
                'message' => $message ? $message : __('auth.login.unauthorized'),
            ]
        ]);
    }
}
