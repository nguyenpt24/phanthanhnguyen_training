<?php

namespace Backend\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LoginRequest
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mail' => 'required|max:64',
            'pwd' => 'required|max:72',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mail.required' => __('validation.mail.required'),
            'mail.max' => __('validation.mail.max'),
            'pwd.required' => __('validation.password.required'),
            'pwd.max' => __('validation.password.max'),
        ];
    }
}
