<?php

namespace Backend\Http\Requests\ReleaseNumber;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReleaseNumberRequest
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'release_number_name' => 'required|max:16|unique:release_numbers,name,' . $this->id,
            'release_number_thumbnail' => 'required',
            'release_number_description' => 'max:500',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     * @return array
     */
    public function messages()
    {
        return [
            'release_number_name.required' => __('validation.release_number.name.required'),
            'release_number_name.max' => __('validation.release_number.name.required'),
            'release_number_name.unique' => __('validation.release_number.name.unique'),
            'release_number_thumbnail.required' => __('validation.release_number.thumbnail.required'),
            'release_number_description.max' => __('validation.release_number.description.max')
        ];
    }
}
