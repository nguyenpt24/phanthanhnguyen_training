<?php

namespace Backend\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PostRequest
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:300|min:10',
            'release_date' => 'required|max:14',
            'status' => 'required',
            'category_parent' => 'required',
            'category_child' => 'required',
            'release_number_id' => 'required',
            'content' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => __('validation.post.title.required'),
            'title.max' => __('validation.post.title.maxmin'),
            'title.min' => __('validation.post.title.maxmin'),
            'release_date.required' => __('validation.post.release_date.required'),
            'release_date.max' => __('validation.post.release_date.max'),
            'status.required' => __('validation.post.status.required'),
            'category_parent.required' => __('validation.post.category_parent.required'),
            'category_child.required' => __('validation.post.category_child.required'),
            'release_number_id.required' => __('validation.post.release_number_id.required'),
            'content.required' => __('validation.post.content.required'),
        ];
    }
}
