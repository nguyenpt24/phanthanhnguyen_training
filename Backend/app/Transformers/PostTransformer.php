<?php

namespace Backend\Transformers;

use Backend\Model\Post;
use League\Fractal\TransformerAbstract;

/**
 * PostTransformer.php
 * Class PostTransformer
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     * @param Post $post
     * @return array
     */
    public function transform(Post $post)
    {
        $array_status = config('constants.status');
        $release_number_name = $post->release_number->name ?? null;
        $thumbnail = $post->image->url ?? null;
        return [
            'id' => $post->id,
            'title' => $post->title,
            'image_id' => $post->image_id,
            'content' => $post->content,
            'release_date' => $post->release_date,
            'status' => $array_status[$post->status] ?? null,
            'user_id' => $post->user_id,
            'category_parent' => $post->category_parent,
            'category_child' => $post->category_child,
            'release_number_id' => $post->release_number_id,
            'release_number_name' => $release_number_name,
            'thumbnail' => $thumbnail,
            'created_at' => $post->created_at,
            'updated_at' => $post->updated_at,
            'author' => $post->user->name ?? null,
            'category_parent_name' => $post->fcategory_parent->name ?? null,
            'category_child_name' => $post->fcategory_child->name ?? null,
        ];
    }

    /**
     * Turn this item object into a generic array
     * @param Post $post
     * @return array
     */
    public function transformShow(Post $post)
    {
        $array_status = config('constants.status');
        return [
            'id' => $post->id,
            'title' => $post->title,
            'image_id' => $post->image_id,
            'content' => $post->content,
            'release_date' => $post->release_date,
            'status' => ['value' => $post->status, 'label' => $array_status[$post->status] ?? null],
            'user_id' => $post->user_id,
            'category_parent' => $post->fcategory_parent,
            'category_child' => $post->fcategory_child,
            'release_number_id' => $post->release_number,
            'thumbnail' => $post->image->url ?? null,
        ];
    }
}
