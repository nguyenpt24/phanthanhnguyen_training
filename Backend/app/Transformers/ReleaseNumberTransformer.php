<?php

namespace Backend\Transformers;

use Backend\Model\ReleaseNumber;
use League\Fractal\TransformerAbstract;

/**
 * ReleaseNumberTransformer.php
 * Class ReleaseNumberTransformer
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumberTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     * @param ReleaseNumber $release_number
     * @return array
     */
    public function transform(ReleaseNumber $release_number)
    {
        $image_url = $release_number->image->url ?? '';
        if ($image_url) {
            $tmp = explode('.', $image_url);
            $ext = end($tmp);
            $thumbnail = $tmp[0] . '_1200x400.' . $ext;
        }
        return [
            'id' => $release_number->id,
            'name' => $release_number->name,
            'description' => $release_number->description,
            'price' => $release_number->price,
            'thumbnail' => $thumbnail ?? '',
            'image_url' => $image_url,
            'image_id' => $release_number->image_id,
        ];
    }
}
