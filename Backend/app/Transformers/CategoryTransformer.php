<?php

namespace Backend\Transformers;

use Backend\Model\Category;
use League\Fractal\TransformerAbstract;

/**
 * CategoryTransformer.php
 * Class CategoryTransformer
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     * @param Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'parent_id' => $category->parent_id,
        ];
    }
}