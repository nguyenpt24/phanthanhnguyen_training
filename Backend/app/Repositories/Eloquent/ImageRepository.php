<?php

namespace Backend\Repositories\Eloquent;

use Backend\Repositories\Contracts\RepositoryInterface;
use Exception;
use Illuminate\Support\Facades\Storage;
use Image;

/**
 * Class ImageRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ImageRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Backend\Model\Image';
    }

    /**
     * Create thumbnail
     * @param string $path
     * @param string $filename
     * @param int $width
     * @param int $height
     * @return string
     */
    public function createThumbnail($path, $filename, $width, $height)
    {
        $wh = $width . 'x' . $height;
        $save_path = str_replace('/public', '/public/thumbnail/' . $wh, str_replace('/images', '', $path));
        $tmp = explode('.', $filename);
        $ext = end($tmp);
        $new_name = $tmp[0] . '_' . $wh . '.' . $ext;
        $path_new = $save_path . '/' . $new_name;
        if (!file_exists($path_new)) {
            $img = Image::make($path . '/' . $filename)->resize($width, $height);
            if (!file_exists($save_path)) {
                mkdir($save_path, 0777, true);
            }
            $img->save($path_new);
        }
        return str_replace(storage_path() . '/app/public', '/storage', $path_new);
    }

    /**
     * Delete file
     * @param object $image
     * @return bool
     */
    public function deleteImage($image)
    {
        try {
            $url = str_replace('/storage/', '/public/', $image->url);
            return Storage::delete($url);
        } catch (Exception $exception) {
            return false;
        }
    }
}
