<?php

namespace Backend\Repositories\Eloquent;

use Backend\Repositories\Contracts\RepositoryInterface;

/**
 * Class CategoryRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Backend\Model\Category';
    }

    /**
     * Retrieve count of repository
     */
    public function countData()
    {
        return $this->_model->count();
    }
}
