<?php

namespace Backend\Repositories\Eloquent;

use Backend\Repositories\Contracts\RepositoryInterface;

/**
 * Class UserRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class UserRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Backend\User';
    }

    /**
     * Retrieve count of repository
     */
    public function countMember()
    {
        return $this->_model->where('role_id',5)->count();
    }
}
