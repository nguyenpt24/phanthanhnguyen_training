<?php

namespace Backend\Repositories\Eloquent;

use Backend\Repositories\Contracts\RepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class PostRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Backend\Model\Post';
    }

    /**
     * Retrieve all data of repository, paginated
     * @param int $pageNumber
     * @return LengthAwarePaginator
     */
    public function paginate($pageNumber)
    {
        $setting = DB::table('settings')->first();
        $per_page = empty($setting->post_page) ? 20 : $setting->post_page;
        return $this->_model
            ->with('image', 'user', 'fcategory_child', 'release_number')
            ->orderBy('id', 'DESC')
            ->paginate($per_page, ['*'], 'page', $pageNumber);
    }

    /**
     * Retrieve all data of repository
     * @return mixed
     */
    public function getAllData()
    {
        return $this->_model->with('image')->get();
    }

    /**
     * Search Method
     * @param array $arSearch
     * @param int $pageNumber
     * @return mixed
     */
    public function search($arSearch, $pageNumber)
    {
        $setting = DB::table('settings')->first();
        $per_page = empty($setting->post_page) ? 20 : $setting->post_page;
        return $this->_model->with('image', 'user', 'fcategory_child', 'release_number')
            ->where(function ($query) use ($arSearch) {
                if (isset($arSearch['release_number']) && $arSearch['release_number'] != '') {
                    $query->where('release_number_id', $arSearch['release_number']);
                }
                if (isset($arSearch['category_parent']) && $arSearch['category_parent'] != '') {
                    $query->where('category_parent', $arSearch['category_parent']);
                }
                if (isset($arSearch['category_child']) && $arSearch['category_child'] != '') {
                    $query->where('category_child', $arSearch['category_child']);
                }
                if (isset($arSearch['status']) && $arSearch['status'] != '') {
                    $query->where('status', $arSearch['status']);
                }
                if (isset($arSearch['content']) && $arSearch['content'] != '') {
                    $query->where(function ($query1) use ($arSearch) {
                        $query1->where('title', 'LIKE', '%' . $arSearch['content'] . '%')
                            ->orWhere('content', 'LIKE', '%' . $arSearch['content'] . '%');
                        return $query1;
                    });
                }
                return $query;
            })
            ->orderBy('id', 'DESC')
            ->paginate($per_page, ['*'], 'page', $pageNumber);
    }

    /**
     * Retrieve count of repository
     */
    public function countData()
    {
        return $this->_model->count();
    }
}
