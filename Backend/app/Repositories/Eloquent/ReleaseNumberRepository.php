<?php

namespace Backend\Repositories\Eloquent;

use Backend\Repositories\Contracts\RepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class ReleaseNumberRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumberRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Backend\Model\ReleaseNumber';
    }

    /**
     * Retrieve all data of repository, paginated
     * @param int $pageNumber
     * @return LengthAwarePaginator
     */
    public function paginate($pageNumber)
    {
        $setting = DB::table('settings')->first();
        $per_page = empty($setting->release_number_page) ? 20 : $setting->release_number_page;
        return $this->_model->with('image')->orderBy('id', 'DESC')->paginate($per_page, ['*'], 'page', $pageNumber);
    }

    /**
     * Retrieve all data of repository
     */
    public function getAllData()
    {
        return $this->_model->with('image')->get();
    }

    /**
     * Retrieve all data of repository
     */
    public function getItemsActive()
    {
        $now = date('Y-m-d');
        return $this->_model
            ->select('*', DB::raw("STR_TO_DATE(name, '%Y\年%m\月%d\日号') as public_date"))
            ->where( DB::raw("STR_TO_DATE(name,'%Y\年%m\月%d\日号')") , '>=', $now)
            ->get();
    }

    /**
     * Retrieve count of repository
     */
    public function countData()
    {
        return $this->_model->count();
    }
}
