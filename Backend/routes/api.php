<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'api.'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register')->name('auth.register');
    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('user', 'AuthController@getAuthenticatedUser')->name('auth.user');
        Route::post('logout', 'AuthController@logout')->name('auth.logout');
     });
});
Route::group(['prefix' => '', 'namespace' => 'ReleaseNumber', 'as' => 'api.'], function () {
    Route::get('release_number/active', 'ReleaseNumberController@isActive')->name('release_number.active');
    Route::get('release_number/info-paginate', 'ReleaseNumberController@infoPaginate')->name('release_number.info_paginate');
    Route::get('release_number/item/{page}', 'ReleaseNumberController@show')->name('release_number.show');
    Route::get('release_number/{page}', 'ReleaseNumberController@paginate')->name('release_number.paginate');
    Route::put('release_number', 'ReleaseNumberController@update')->name('release_number.update');
    Route::resource('release_number', 'ReleaseNumberController', ['except' => ['create', 'edit', 'update', 'show']]);
});
Route::group(['prefix' => '', 'namespace' => 'Image', 'as' => 'api.'], function () {
    Route::post('image', 'ImageController@store')->name('image.update');
    Route::delete('image/{id}', 'ImageController@destroy')->name('image.destroy');
    Route::get('image/{id}/{w}x{h}', 'ImageController@getImage')->name('image.get_image');
});
Route::group(['prefix' => '', 'namespace' => 'Post', 'as' => 'api.'], function () {
    Route::get('post/info-paginate', 'PostController@infoPaginate')->name('post.info_paginate');
    Route::get('post/item/{page}', 'PostController@show')->name('post.show');
    Route::get('post', 'PostController@search')->name('post.search');
    Route::get('post/{page}', 'PostController@paginate')->name('post.paginate');
    Route::put('post', 'PostController@update')->name('post.update');
    Route::resource('post', 'PostController', ['except' => ['edit', 'update', 'show', 'index'] ]);
});
Route::group(['prefix' => '', 'namespace' => 'Category', 'as' => 'api.'], function () {
    Route::get('category', 'CategoryController@index')->name('image.index');
});
Route::group(['as' => 'api.'], function () {
    Route::get('home', 'HomeController@index')->name('home.index');
});