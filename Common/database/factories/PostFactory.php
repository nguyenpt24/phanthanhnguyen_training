<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $filepath = storage_path('app/public/media/files/posts');
    if(!file_exists($filepath)){
        Storage::makeDirectory($filepath);
    }
    return [
        'title' => $faker->text($maxNbChars = 200),
        'content' => $faker->realText,
        'thumbnail' => $faker->image($filepath,400,300,'business',false),
        'status' => 1,
        'users_id' => 1,
        'category_parent' => 1,
        'category_child' => 7,
    ];
});
