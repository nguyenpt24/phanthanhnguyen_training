<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('mail',255)->unique();
            $table->string('password',255);
            $table->string('phone',12)->nullable();
            $table->string('birthday',10)->nullable();
            $table->integer('gender')->length(1)->unsigned()->nullable();
            $table->string('address',100)->nullable();
            $table->integer('status')->length(1)->unsigned()->nullable();
            $table->integer('role_id')->length(11)->unsigned();
            $table->integer('sort')->length(11)->unsigned()->default(5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
