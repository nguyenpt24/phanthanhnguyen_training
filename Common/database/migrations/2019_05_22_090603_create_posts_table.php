<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',300);
            $table->longText('content');
            $table->string('release_date',10);
            $table->string('status',50);
            $table->integer('image_id')->length(11)->unsigned();
            $table->integer('user_id')->length(11)->unsigned();
            $table->integer('category_parent')->length(11)->unsigned();
            $table->integer('category_child')->length(11)->unsigned();
            $table->integer('release_number_id')->length(11)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
