<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->integer('release_number_page')->length(3)->unsigned()->default(20);
            $table->integer('user_admin_page')->length(3)->unsigned()->default(20);
            $table->integer('post_page')->length(3)->unsigned()->default(20);
            $table->integer('member_page')->length(3)->unsigned()->default(20);
            $table->integer('img_page')->length(3)->unsigned()->default(20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
