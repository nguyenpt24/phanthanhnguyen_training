<?php

use Illuminate\Database\Seeder;
use Frontend\Model\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'Admin Super';
        $admin->email = 'admin01@gmail.com';
        $admin->password = bcrypt('123123');
        $admin->save();

        $editor = new User();
        $editor->name = 'Ngọc Trinh';
        $editor->email = 'editor01@gmail.com';
        $editor->password = bcrypt('123123');
        $editor->save();

        $user = new User();
        $user->name = 'Sơn Tùng';
        $user->email = 'user01@gmail.com';
        $user->password = bcrypt('123123');
        $user->save();
    }
}
