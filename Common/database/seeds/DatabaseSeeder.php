<?php

use Illuminate\Database\Seeder;
use Backend\Model\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(SettingsTableSeeder::class);
         $this->call(UserTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
//         $this->call(PostTableSeeder::class);
    }

}

Class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'superadmin', 'description' => 'Super Admin'],
            ['name' => 'admin', 'description' => 'Admin'],
            ['name' => 'editor', 'description' => 'Editor'],
            ['name' => 'contributer', 'description' => 'Contributer']
        ]);
    }

}

Class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->insert([
            [ 'release_number_page' => 20, 'user_admin_page' => 20,'post_page' => 20,'member_page' => 20,'img_page' => 20]
        ]);
    }

}

Class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [ 'name'=>'NguyenIT', 'mail'=>'ptnbk2401@gmail.com', 'password'=>bcrypt('123123'),'status' => 1,'roles_id' => 1,'sort' => 1],
            [ 'name'=>'Văn A', 'mail'=>'admin@gmail.com', 'password'=>bcrypt('123123'),'status' => 1,'roles_id' => 2,'sort' => 2],
            [ 'name'=>'Thị B', 'mail'=>'editor@gmail.com', 'password'=>bcrypt('123123'),'status' => 1,'roles_id' => 2,'sort' => 3 ],
            [ 'name'=>'Thanh C', 'mail'=>'contributer@gmail.com', 'password'=>bcrypt('123123'),'status' => 1,'roles_id' => 2,'sort' => 4 ]
        ]);
    }

}

Class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->insert([
            [ 'name'=>'親カテゴリ1', 'global_display'=>1, 'menu_display'=>1, 'parent_id' => 0 ],
            [ 'name'=>'親カテゴリ2', 'global_display'=>1, 'menu_display'=>1, 'parent_id' => 0 ],
            [ 'name'=>'親カテゴリ3', 'global_display'=>0, 'menu_display'=>1, 'parent_id' => 0 ],
            [ 'name'=>'親カテゴリ4', 'global_display'=>0, 'menu_display'=>1, 'parent_id' => 0 ],
            [ 'name'=>'親カテゴリ5', 'global_display'=>1, 'menu_display'=>1, 'parent_id' => 0 ],
            [ 'name'=>'子カテゴリ1', 'global_display'=>0, 'menu_display'=>0, 'parent_id' => 1 ],
            [ 'name'=>'子カテゴリ2', 'global_display'=>1, 'menu_display'=>0, 'parent_id' => 1 ],
            [ 'name'=>'子カテゴリ3', 'global_display'=>1, 'menu_display'=>0, 'parent_id' => 1 ],
            [ 'name'=>'子カテゴリ4', 'global_display'=>1, 'menu_display'=>0, 'parent_id' => 2 ],
            [ 'name'=>'子カテゴリ5', 'global_display'=>0, 'menu_display'=>0, 'parent_id' => 2 ],
            [ 'name'=>'子カテゴリ6', 'global_display'=>0, 'menu_display'=>0, 'parent_id' => 2 ],
            [ 'name'=>'子カテゴリ7', 'global_display'=>0, 'menu_display'=>0, 'parent_id' => 3 ],
            [ 'name'=>'子カテゴリ8', 'global_display'=>1, 'menu_display'=>0, 'parent_id' => 3 ],
            [ 'name'=>'子カテゴリ9', 'global_display'=>0, 'menu_display'=>0, 'parent_id' => 3 ],
        ]);
    }

}

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 10)->create();
    }
}

