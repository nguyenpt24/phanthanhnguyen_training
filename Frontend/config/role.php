<?php

return [
    'superadmin' => [
        'id' => 1,
        'name' => 'superadmin'
    ],
    'admin' => [
        'id' => 2,
        'name' => 'admin'
    ],
    'editor' => [
        'id' => 3,
        'name' => 'editor'
    ],
    'contributor' => [
        'id' => 4,
        'name' => 'contributor'
    ],
    'member' => [
        'id' => 5,
        'name' => 'member'
    ],
];
