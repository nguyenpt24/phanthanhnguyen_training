<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/category/{id}', 'Category\CategoryController@index')->name('category.index');
Route::get('/release-number/{id}', 'ReleaseNumber\ReleaseNumberController@index')->name('release_number.index');
Route::group(['prefix' => '', 'namespace' => 'Post'], function () {
    Route::get('/blog/{id}', 'PostController@index')->name('post.index');
    Route::get('/search', 'PostController@search')->name('post.search');
});
Route::group(['prefix' => 'ajax', 'namespace' => 'Ajax'], function () {
    Route::get('/home-post', 'PostController@homeIndex')->name('ajax.home');
    Route::get('/category', 'PostController@categoryIndex')->name('ajax.category.index');
    Route::get('/release-number', 'PostController@releaseNumberIndex')->name('ajax.release_number.index');
    Route::get('/search', 'PostController@search')->name('ajax.post.search');
    Route::post('/auth-login', 'AuthController@login')->name('ajax.auth.login');
    Route::post('/auth-register', 'AuthController@register')->name('ajax.auth.register');
});
Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('logout', 'LoginController@logout')->name('logout');
    Route::post('register', 'RegisterController@register')->name('register');
});
