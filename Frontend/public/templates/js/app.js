$(document).ready(function () {
    /**
     * Show|Hide dropdown category when hover
     */
    $('.header-menu .nav-item.dropdown').hover(function () {
        showDropdown(this)
    }, function () {
        closeDropdown(this)
    });

    /**
     * Config datepicker inputBirthday
     */
    $('#inputBirthday').datepicker({
        uiLibrary: 'bootstrap4',
        showRightIcon: false,
        format: 'yyyy/mm/dd',
        maxDate: new Date(),
    })
        .change(dateChanged)
        .on('changeDate', dateChanged);

    /**
     * Config Carousel slideHome
     */
    $('#slideHome').carousel({
        interval: 5000000,
        wrap: true
    });

    var fontSize = $.cookie('saveFontSize');
    if (fontSize) {
        checkedCheckBoxFontSize(fontSize)
        setFontSize();
    }

    /**
     * Change font size when change font size from checkbox
     */
    $('.fontsize').change(function () {
        var fontsize = getFontSize();
        var timeout = 10; // Expires in 10 days
        createFontSizeCookie(timeout, fontsize)
        setFontSize();
    });

    /**
     * Validate login
     */
    validateLogin();

    /**
     * Validate register
     */
    validateRegister();

    $('.show-mb .menubar').click(function () {
        if ($(this).hasClass('open')) {
            hideMenubarMobile();
        } else {
            showMenubarMobile();
        }
    });

    $('.nav-item.dropdown').after().click(function () {
        $(this).toggleClass('show-mb');
        $(this).find('.dropdown-menu').toggleClass('show-mb');
        $('.nav-item.dropdown').not(this).removeClass('show-mb');
        $('.nav-item.dropdown').not(this).find('.dropdown-menu').removeClass('show-mb');
        // if($(this).hasClass('show-mb')) {
        //     $(this).find('.dropdown-menu').addClass('show-mb');
        // } else {
        //     $(this).find('.dropdown-menu').removeClass('show-mb');
        // }
    })

    responsive();
});

/**
 * Show dropdown
 * @param element
 */
function showDropdown(element) {
    if($(window).width() > 768) {
        $(element).addClass('show');
        $(element).find('.dropdown-menu').addClass('show');
    }
}

/**
 * Close dropdown
 * @param element
 */
function closeDropdown(element) {
    if($(window).width() > 768) {
        $(element).removeClass('show');
        $(element).find('.dropdown-menu').removeClass('show');
    }
}

/**
 * Set font-size html
 */
function setFontSize() {
    var fontSize = $.cookie('saveFontSize');
    if (fontSize) {
        $('html').css('font-size', $.cookie('saveFontSize') + 'px');
    }
}

/**
 * Get font-size from checkbox fontsize
 * @returns {*|void|jQuery} fontSize
 */
function getFontSize() {
    var fontSize = $('input[type=radio][name=fontsize]:checked').val();
    return fontSize;
}

/**
 * Create Cookie font size
 * @param {number} timeout
 * @param {number} fontsize
 */
function createFontSizeCookie(timeout, fontsize) {
    $.cookie('saveFontSize', fontsize, {
        expires: timeout,
        path: '/'
    });
}

/**
 * Checked checkbox font size
 * @param {number} fontsize
 */
function checkedCheckBoxFontSize(fontsize) {
    $('input[type=radio][name=fontsize]').attr('checked', false);
    $('.label-fontsize').removeClass('active');
    var id = fontSize == 13 ? 'labelSizeS' : (fontSize == 17 ? 'labelSizeB' : 'labelSizeM');
    $('input[type=radio][name=fontsize][value=' + fontSize + ']').attr('checked', 'checked');
    $('#' + id).addClass('active');
}

/**
 * Validate Login
 */
function validateLogin() {
    ajax_sendding = false;
    var login_fail = $('#login-fail');
    var submit = $('#login-form');
    submit.submit(function () {
        $('.invalid-feedback strong').text('');
        $('.form-control').removeClass('is-invalid');
        login_fail.hide();
        if (ajax_sendding === true) {
            return false;
        }
        let token = $('input[name=_token]').val();
        let mail = $('#email').val();
        let password = $('#password').val();
        ajax_sendding = true;
        $('#Loading').addClass('loading');
        $.ajax({
            url: '/ajax/auth-login',
            type: 'POST',
            cache: false,
            data: {_token: token, mail: mail, password: password},
            success: function (data) {
                if (data.data) {
                    location.reload();
                } else {
                    login_fail.show();
                    login_fail.find('strong').text(data.error.message);
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                if (!!errors.mail) {
                    $('#invalid-feedback-mail strong').text(errors.mail);
                    $('#email').addClass('is-invalid');
                }
                if (!!errors.password) {
                    $('#invalid-feedback-password strong').text(errors.password);
                    $('#password').addClass('is-invalid');
                }
            }
        }).always(function () {
            $('#Loading').removeClass('loading');
            ajax_sendding = false;
        });
        return false;
    });
}

/**
 * Validate Register
 */
function validateRegister() {
    ajax_sendding = false;
    var submit = $('#submit-register');
    submit.click(function () {
        if (ajax_sendding === true) {
            return false;
        }
        let token = $('input[name=_token]').val();
        let name = $('#staticName').val();
        let mail = $('#staticEmail').val();
        let password = $('#inputPassword').val();
        let birthday = $('#inputBirthday').val();
        let gender = $('input[name=gender]:checked').val();
        ajax_sendding = true;
        $('#registerLoading').addClass('loading');
        $.ajax({
            url: '/ajax/auth-register',
            type: 'POST',
            cache: false,
            data: {_token: token, name: name, mail: mail, password: password, birthday: birthday, gender: gender},
            success: function (data) {
                if (data.data) {
                    location.reload();
                } else {
                    console.log(data.error)
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                $('.invalid-feedback strong').text('');
                $('.form-control').removeClass('is-invalid');
                if (!!errors.name) {
                    $('#invalid-feedback-register-name strong').text(errors.name);
                    $('#staticName').addClass('is-invalid');
                }
                if (!!errors.mail) {
                    $('#invalid-feedback-register-mail strong').text(errors.mail);
                    $('#staticEmail').addClass('is-invalid');
                }
                if (!!errors.password) {
                    $('#invalid-feedback-register-password strong').text(errors.password);
                    $('#inputPassword').addClass('is-invalid');
                }
                if (!!errors.birthday) {
                    $('#invalid-feedback-register-birthday strong').text(errors.birthday);
                    $('#inputPassword').addClass('is-invalid');
                }
            }
        }).always(function () {
            $('#registerLoading').removeClass('loading');
            ajax_sendding = false;
        });
        return false;
    });
}

/**
 * Validate date when change
 */
function dateChanged(ev) {
    let val = $(this).val();
    if (!moment(val, 'YYYY/MM/DD', true).isValid()) {
        $(this).val(moment().format('YYYY/MM/DD'));
    }
}

/**
 * Show menu mobile
 */
function showMenubarMobile() {
    $('.menubar').addClass('open');
    $('.category-top').addClass('show');
}

/**
 * Hide menu mobile
 */
function hideMenubarMobile() {
    $('.menubar').removeClass('open');
    $('.category-top').removeClass('show');
}

/**
 * Responsive
 */
function responsive() {
    responsiveSlide();
    responsivePostLatest();
    responsivePostContent()
}

/**
 * Add/Remove class show-mb Slide
 */
function responsiveSlide() {
    let element = $('#slideHome');
    responsiveID(element)
}

/**
 * Add/Remove class show-mb Post Latest
 */
function responsivePostLatest() {
    let element = $('#content-latest-posts');
    responsiveID(element)
}

/**
 * Add/Remove class show-mb Post Content
 */
function responsivePostContent() {
    let element = $('#content-posts');
    responsiveID(element)
}

function responsiveID(element) {
    if ($(window).width() < 769) {
        $(element).addClass('show-mb');
    } else {
        $(element).removeClass('show-mb');
    }
    $(window).on('resize', function(){
        var win = $(this); //this = window
        if (win.width() < 769) {
            $(element).addClass('show-mb');
        } else {
            $(element).removeClass('show-mb');
        }
    });
}