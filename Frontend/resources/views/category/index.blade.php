@extends('templates.master')

@section('content')
    <div class="container category">
        <div id="content-posts" data-category-id="{{$category->id}}" data-total="{{$list_posts->total()}}">
            <h2 class="h-meta">{{$category->name}}ニュース一覧</h2>
            <div class="row content">
                @if(!empty($list_posts) && count($list_posts) )
                    @widget('Category\CategoryIndex',['list_items'=>$list_posts])
                @else
                    <div class="text-center p-2" style="flex-grow: 1">データなし</div>
                @endif
            </div>
            @if($list_posts->lastPage() != 1)
                <div class="bottom" data-last-page="{{$list_posts->lastPage()}}" id="btn-bottom">
                    <a href="javascript:;" class="btn btn-outline-secondary load-more" id="show_other">もっと見る</a>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        var page = 2;
        var category_id = $('#content-posts').attr('data-category-id');
        var last_page = $('#btn-bottom').attr('data-last-page');
        ajax_sendding = false;
        var show_other = $('#show_other');
        show_other.click(function () {
            if (ajax_sendding == true) {
                return false;
            }
            ajax_sendding = true;
            show_other.addClass('loading');
            $.ajax({
                url: "{{ route('ajax.category.index') }}",
                type: 'GET',
                cache: false,
                data: {page: page, category_id: category_id},
                success: function (data) {
                    $('#content-posts .content').append(data);
                    page += 1;
                },
                error: function () {
                }
            }).always(function () {
                show_other.removeClass('loading');
                ajax_sendding = false;
                if (last_page < page) {
                    $('#btn-bottom').hide();
                }
            });
            return false;
        });
    </script>
@stop
