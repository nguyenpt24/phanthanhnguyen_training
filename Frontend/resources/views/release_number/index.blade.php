@extends('templates.master')

@section('content')
    <div class="container release-number">
        <div id="content-posts" data-release-number-id="{{$release_number->id}}" data-total="{{$list_posts->total()}}">
            <div class="row content-header">
                @widget('Image\GetImage',['image'=>$release_number->image,'size'=>'1110x400','class'=>'release-img'])
                <div class="release-name">{{$release_number->name}}</div>
            </div>
            <div class="release-description">{{$release_number->description}}</div>
            @php
                $pay = false;
                if(Auth::check()) {
                    $user_auth = Auth::user();
                    foreach($release_number->users_paid as $user) {
                        if($user->id == $user_auth->id ) {
                            $pay = true;
                        }
                    }
                }
            @endphp
            @if($pay)
                <div class="row content">
                    @if(!empty($list_posts) && count($list_posts) )
                        @widget('ReleaseNumber\ReleaseNumberIndex',['list_items'=>$list_posts])
                    @else
                        <div class="text-center p-2" style="flex-grow: 1">データなし</div>
                    @endif
                </div>
                @if($list_posts->lastPage() != 1)
                    <div class="bottom" data-last-page="{{$list_posts->lastPage()}}" id="btn-bottom">
                        <a href="javascript:;" class="btn btn-outline-secondary load-more load-more-rn" id="show_other">もっと見る</a>
                    </div>
                @endif
            @else
                <div class="row content">
                    <div class="block-pay">この発売号を買う</div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        var page = 2;
        var release_number_id = $('#content-posts').attr('data-release-number-id');
        var last_page = $('#btn-bottom').attr('data-last-page');
        ajax_sendding = false;
        var show_other = $('#show_other');
        show_other.click(function () {
            if (ajax_sendding == true) {
                return false;
            }
            ajax_sendding = true;
            show_other.addClass('loading');
            $.ajax({
                url: "{{ route('ajax.release_number.index') }}",
                type: 'GET',
                cache: false,
                data: {page: page, release_number_id: release_number_id},
                success: function (data) {
                    $('#content-posts .content').append(data);
                    page += 1;
                },
                error: function () {
                }
            }).always(function () {
                show_other.removeClass('loading');
                ajax_sendding = false;
                if (last_page < page) {
                    $('#btn-bottom').hide();
                }
            });
            return false;
        });
    </script>
@stop
