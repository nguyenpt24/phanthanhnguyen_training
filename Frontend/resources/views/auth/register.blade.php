<div class="modal fade" id="register-popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">新規メンバー登録</h5>
                <button type="button" class="btn btn-close-popup close" data-dismiss="modal" aria-label="Close">閉じる
                </button>
            </div>
            <div class="modal-body">
                <form id="register-form" action="javascript:void(0)">
                    @csrf
                    <div class="form-group row">
                        <label for="staticName" class="col-sm-3 col-form-label">ユーザー名</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control"
                                   id="staticName" placeholder="ユーザー名の入力">
                            <span class="invalid-feedback" id="invalid-feedback-register-name" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label">メールアドレス</label>
                        <div class="col-sm-9">
                            <input type="text" name="mail" class="form-control"
                                   id="staticEmail" placeholder="メールの入力">
                            <span class="invalid-feedback" id="invalid-feedback-register-mail" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">パスワード</label>
                        <div class="col-sm-9">
                            <input type="password" name="password"
                                   class="form-control" id="inputPassword"
                                   placeholder="パスワードの入力">
                            <span class="invalid-feedback" id="invalid-feedback-register-password" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBirthday" class="col-sm-3 col-form-label">生年月日</label>
                        <div class="col-sm-9">
                            <input type="text" name="birthday"
                                   class="form-control" id="inputBirthday"
                                   placeholder="生年月日の入力" autocomplete="off">
                            <span class="invalid-feedback" id="invalid-feedback-register-birthday" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">性別</label>
                        <div class="col-sm-9">
                            <div class="custom-control custom-radio">
                                <input type="radio" checked class="custom-control-input" id="customControlValidation1"
                                       name="gender" value="1">
                                <label class="custom-control-label" for="customControlValidation1">男</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customControlValidation2"
                                       name="gender" value="0">
                                <label class="custom-control-label" for="customControlValidation2">女</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="registerLoading"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button form="register-form" type="button" class="btn mod-btn-submit" id="submit-register">登録</button>
                <button type="button" class="btn mod-btn-cancel" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>
