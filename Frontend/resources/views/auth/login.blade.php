<!-- Modal -->
<div class="modal fade" id="login-popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ログイン</h5>
                <button type="button" class="btn btn-close-popup close" data-dismiss="modal" aria-label="Close">閉じる
                </button>
            </div>
            <div class="modal-body">
                <form id="login-form" action="javascript:;" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label">メールアドレス</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" autofocus
                                   placeholder="メールの入力" name="mail" value="{{ old('mail') }}" autocomplete="mail">
                            <span class="invalid-feedback" role="alert" id="invalid-feedback-mail">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">パスワード</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control"
                                   id="password" placeholder="パスワードの入力" name="password">
                            <span class="invalid-feedback" role="alert" id="invalid-feedback-password">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row" id="Loading">
                        <div class="alert alert-danger" role="alert" id="login-fail">
                            <strong></strong>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button form="login-form" type="submit" class="btn mod-btn-submit" id="submit-login">ログイン</button>
                <button type="button" class="btn mod-btn-cancel" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>
