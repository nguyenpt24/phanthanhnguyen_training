@extends('templates.master')

@section('content')
    <div class="container">
        <div class="home-slider">
            @include('templates.slider.slidehome')
        </div>
        <div class="latest-posts">
            @include('templates.home.latest_posts')
        </div>
        <div class="block-posts">
            @include('templates.home.posts')
        </div>
    </div>
@endsection
