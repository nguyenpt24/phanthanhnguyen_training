<footer>
    <div class="footer text-center">
        <p>Copyrights © 2019 <a href="#">Rikkeisoft</a> All Rights Reserved.</p>
    </div>
</footer>
</div>
@include('auth.login')
@include('auth.register')
<script src="/templates/js/popper.min.js"></script>
<script src="/templates/js/bootstrap.min.js"></script>
<script src="/templates/js/gijgo.min.js" type="text/javascript"></script>
<script src="/templates/js/moment.js" type="text/javascript"></script>
<script src="/templates/js/app.js"></script>
@yield('js')
</body>
</html>
