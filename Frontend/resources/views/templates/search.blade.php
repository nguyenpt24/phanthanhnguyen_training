<form class="form-inline" action="{{route('post.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="検索内容の入力。。。" autocomplete="off"
           aria-label="Search" name="s" value="{{request('s') ?? ''}}">
    <button class="btn my-2 my-sm-0 btn-submit" type="submit">検索</button>
</form>
