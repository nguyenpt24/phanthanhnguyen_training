<div id="content-posts">
    <h2 class="h-meta">記事</h2>
    <div class="row content">
        @widget('Post\PostIndex',['list_items'=>$list_posts])
    </div>
    @if($list_posts->lastPage() != 1)
        <div class="bottom" data-last-page="{{$list_posts->lastPage()}}" id="btn-bottom">
            <a href="javascript:;" class="btn btn-outline-secondary load-more" id="show_other">もっと見る</a>
        </div>
    @endif
</div>

@section('js')
    <script>
        var page = 2;
        ajax_sendding = false;
        var show_other = $('#show_other');
        var last_page = $('#btn-bottom').attr('data-last-page');
        show_other.click(function () {
            if (ajax_sendding == true){
                return false;
            }
            ajax_sendding = true;
            show_other.addClass('loading');
            $.ajax({
                url: "{{ route('ajax.home') }}",
                type: 'GET',
                cache: false,
                data: {page: page},
                success: function(data){
                    $('#content-posts .content').append(data);
                    page += 1;
                },
                error: function (){
                }
            }).always(function(){
                show_other.removeClass('loading');
                ajax_sendding = false;
                if (last_page < page) {
                    $('#btn-bottom').hide();
                }
            });
            return false;
        });
    </script>
@stop
