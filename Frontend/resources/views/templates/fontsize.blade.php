<div class="btn-group btn-group-toggle" data-toggle="buttons">
    <label class="btn btn-secondary label-fontsize" id="labelSizeS">
        <input type="radio" class="fontsize" name="fontsize" id="small-size" value="13" autocomplete="off">小
    </label>
    <label class="btn btn-secondary active label-fontsize" id="labelSizeM">
        <input type="radio" class="fontsize" name="fontsize" id="medium-size" value="15" autocomplete="off" checked>中
    </label>
    <label class="btn btn-secondary label-fontsize" id="labelSizeB">
        <input type="radio" class="fontsize" name="fontsize" id="big-size" value="17" autocomplete="off">大
    </label>
</div>
<script>
    var fontSizeCookie = $.cookie("saveFontSize");
    var fontSize = fontSizeCookie ? fontSizeCookie : 15;
    document.querySelector('html').style.fontSize = fontSize + 'px';
</script>
