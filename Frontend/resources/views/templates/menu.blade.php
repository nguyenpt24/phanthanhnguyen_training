<div class="category-top">
    @widget('Category\CategoryMenu')
</div>
<div class="category-global">
    @widget('Category\CategoryGlobalMenu')
</div>
