<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Site</title>
    <link rel="shortcut icon" href="/templates/images/favicon.ico">
    <link rel="stylesheet" href="/templates/css/css/bootstrap.min.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/templates/css/css/app.css">
    <script src="/templates/js/jquery.min.js"></script>
    <script src="/templates/js/jquery.cookie.js" type="text/javascript"></script>
    {{--<script src="/templates/fontawesome/js/all.min.js" data-auto-replace-svg="nest"></script>--}}
    @yield('css')
</head>
<body>
<div class="container-fluid">
    <header>
        <div class="header text-center">
            <div class="header-top">
                <div class="header-top-right float-right">
                    <ul class="user-auth d-flex">
                        @if(!\Auth::check())
                            <li class="register"><a data-toggle="modal" data-target="#register-popup">メンバー登録</a></li>
                            <li class="login"><a data-toggle="modal" data-target="#login-popup">ログイン</a></li>
                        @else
                            <li class="name">
                                <span>{{\Auth::user()->name}}</span>
                            </li>
                            <li class="logout">
                                <form class="form-inline" action="{{route('logout')}}" method="post" id="logout-form">
                                    @csrf
                                    <a onclick="document.getElementById('logout-form').submit()">ログアウト</a>
                                </form>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="header-middle header-middle-mobile container d-flex">
                <div class="logo">
                    <a href="/">総合ジャーナル</a>
                </div>
                <div class="search-box">
                    <nav class="navbar navbar-light ">
                        @include('templates.search')
                    </nav>
                </div>
                <div class="resize-box">
                    <div class="show-pc">
                        <p>文字サイズ</p>
                        @include('templates.fontsize')
                    </div>
                    <div class="show-mb">
                        <a href="javascript:;" class="menubar"></a>
                    </div>
                </div>
            </div>
            <div class="header-menu container">
                @include('templates.menu')
            </div>
        </div>
    </header>
