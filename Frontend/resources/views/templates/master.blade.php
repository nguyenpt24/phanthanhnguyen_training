@include('templates.header')
<main>
    @yield('content')
</main>
@include('templates.footer')
