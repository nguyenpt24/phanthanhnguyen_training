@php
    $class = $config['class'] ?? '';
@endphp
<img class="d-block w-100 {{$class}}" src="{{$url_image}}">
