<div class="carousel-inner">
    @foreach($list_items as $key=>$item)
        @php
            $active = (!$key) ? 'active' : '';
        @endphp
        <div class="carousel-item {{$active}}">
            <div class="row">
                <div class="col col-4 info">
                    <p class="meta">発売号</p>
                    <h3 class="name"><a href="{{route('release_number.index',$item->id)}}">{{$item->name}}</a></h3>
                    <div class="description">
                        <p class="description-content">
                            {{$item->description}}
                        </p>
                    </div>
                    <div class="control">
                        <a class="btn-next" href="#slideHome" role="button" data-slide="next">次</a>
                    </div>
                </div>
                <div class="col col-8 image">
                    <a href="{{route('release_number.index',$item->id)}}">
                        @widget('Image\GetImage',['image'=>$item->image,'size'=>'925x410'])
                    </a>
                </div>
            </div>
        </div>
    @endforeach
</div>
