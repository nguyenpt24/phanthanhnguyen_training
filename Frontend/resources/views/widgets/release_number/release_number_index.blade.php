@foreach($list_items as $item)
    <div class="col-6 post-item">
        @widget('Image\GetImage',['image'=>$item->image,'size'=>'540x263','class'=>'post-img'])
        <div class="d-info">
            <h2 class="h-title"><a href="{{route('post.index',$item->id)}}" title="{{$item->title}}">{{ $item->title }}</a></h2>
            <div class="p-preview">
                <p>{!! str_limit(strip_tags($item->content), $limit = 70, $end = '.') !!}</p>
                <a href="{{route('post.index',$item->id)}}" class="link-detail"></a>
            </div>
        </div>
    </div>
@endforeach