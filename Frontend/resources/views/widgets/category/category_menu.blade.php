<ul class="nav">
    <li class="nav-item"><a
                class="nav-link {{(strpos(Route::currentRouteName(), 'home.index') === 0) ? 'active' : '' }}" href="/">ホーム</a>
    </li>
    @foreach($list_items as $item)
        @php
            $active = (Request::url() === route('category.index',$item->id) ) ? 'active' : '';
        @endphp
        @if( !count($item->childes) )
            <li class="nav-item"><a class="nav-link {{$active}}"
                                    href="{{route('category.index',$item->id)}}">{{$item->name}}</a></li>
        @else
            <li class="nav-item dropdown">
                <a class="nav-link {{$active}}" href="{{route('category.index',$item->id)}}" role="button"
                   aria-haspopup="true" aria-expanded="false">{{$item->name}}</a>
                <div class="dropdown-menu">
                    @foreach($item->childes as $iteme)
                        <a class="dropdown-item" href="{{route('category.index',$iteme->id)}}">{{$iteme->name}}</a>
                    @endforeach
                </div>
            </li>
        @endif
    @endforeach
</ul>
