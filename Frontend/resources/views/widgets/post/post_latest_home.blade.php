@if(count($list_items))
    <div class="row content">
        @php
            $top1 = $list_items{0};
        @endphp
        <div class="top-1 item">
            @widget('Image\GetImage',['image'=>$top1->image, 'size'=>'316x415', 'class'=>'post-img'])
            <div class="d-info">
                <h2 class="h-title">
                    <a href="{{route('post.index',$top1->id)}}" title="{{$top1->title}}">{{ $top1->title }}</a>
                </h2>
                <p class="p-created">{{ date('Y\年m\月d\日', strtotime($top1->release_date)) }}</p>
            </div>
        </div>
        <div class="top-234">
            @if(!empty($list_items{1}))
                @php
                    $top2 = $list_items{1};
                @endphp
                <div class="row top-2 item">
                    @widget('Image\GetImage',['image'=>$top2->image, 'size'=>'607x204', 'class'=>'post-img'])
                    <div class="d-info">
                        <h2 class="h-title">
                            <a href="{{route('post.index',$top2->id)}}" title="{{$top2->title}}">{{ $top2->title }}</a>
                        </h2>
                        <p class="p-created">{{ date('Y\年m\月d\日', strtotime($top2->release_date)) }}</p>
                    </div>
                </div>
            @endif
            <div class="row top-34">
                @if(!empty($list_items{2}))
                    @php
                        $top3 = $list_items{2};
                    @endphp
                    <div class="col top-3 item">
                        @widget('Image\GetImage',['image'=>$top3->image, 'size'=>'299x205', 'class'=>'post-img'])
                        <div class="d-info">
                            <h2 class="h-title">
                                <a href="{{route('post.index',$top3->id)}}" title="{{$top3->title}}">{{ $top3->title }}</a>
                            </h2>
                            <p class="p-created">{{ date('Y\年m\月d\日', strtotime($top3->release_date)) }}</p>
                        </div>
                    </div>
                @endif
                @if(!empty($list_items{3}))
                    @php
                        $top4 = $list_items{3};
                    @endphp
                    <div class="col top-4 item">
                        @widget('Image\GetImage',['image'=>$top4->image, 'size'=>'299x205', 'class'=>'post-img'])
                        <div class="d-info">
                            <h2 class="h-title">
                                <a href="{{route('post.index',$top4->id)}}" title="{{$top4->title}}">{{ $top4->title }}</a>
                            </h2>
                            <p class="p-created">{{ date('Y\年m\月d\日', strtotime($top4->release_date)) }}</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif
