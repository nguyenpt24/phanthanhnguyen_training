@extends('templates.master')

@section('content')
    <div class="container article">
        <div id="content-post" data-category-id="{{$post->id}}" data-release="{{$post->release_number_id}}">
            <h1 class="post-title">{{$post->title}}</h1>
            <div class="content">
                @php
                    $pay = false;
                    if(Auth::check()) {
                        $user_auth = Auth::user();
                        if($user_auth->id == $author->id){
                            $pay = true;
                        } else {
                            $release_number = $post->release_number;
                            foreach($release_number->users_paid as $user) {
                                if($user->id == $user_auth->id ) {
                                    $pay = true;
                                }
                            }
                        }
                    }
                @endphp
                @if($pay)
                    {!! $post->content !!}
                @else
                    <p>{!! str_limit(strip_tags($post->content), $limit = 200, $end = '.') !!}</p>
                    <div class="block-view">
                        <p class="message">発売号の記事は決済実行が必要です。</p>
                    </div>
                @endif
            </div>
            <div class="post-meta">
                <p class="author">作成者：<span>{{$author->name}}</span></p>｜<p class="created_at">
                    作成日：<span>{{ date('Y\年m\月d\日', strtotime($post->created_at)) }}</span></p>
            </div>
        </div>
    </div>
@endsection
