@extends('templates.master')

@section('content')
    <div class="container category">
        <div id="content-posts">
            <h2  class="h-meta" style="color: #fd7e0e;">約{{$list_posts->total()}}件</h2>
            <div class="row content">
                @if(!empty($list_posts) && count($list_posts) )
                    @widget('Post\Search',['list_items'=>$list_posts])
                @else
                    <div style="flex-grow: 1; font-size: 1.25rem; padding: 10px 20px;">検索結果がありません。</div>
                @endif
            </div>
            @if($list_posts->lastPage() != 1)
                <div class="bottom" data-last-page="{{$list_posts->lastPage()}}" id="btn-bottom">
                    <a href="javascript:;" class="btn btn-outline-secondary load-more" id="show_other">もっと見る</a>
                </div>
            @endif
            <input type="hidden" name="hidden_search_content" value="{{request('s')??''}}">
        </div>
    </div>
@endsection

@section('js')
    <script>
        var page = 2;
        var search_content = $('input[name=hidden_search_content]').val();
        var last_page = $('#btn-bottom').attr('data-last-page');
        ajax_sendding = false;
        var show_other = $('#show_other');
        show_other.click(function () {
            if (ajax_sendding == true) {
                return false;
            }
            ajax_sendding = true;
            show_other.addClass('loading');
            $.ajax({
                url: "{{ route('ajax.post.search') }}",
                type: 'GET',
                cache: false,
                data: {page: page, search_content: search_content},
                success: function (data) {
                    $('#content-posts .content').append(data);
                    page += 1;
                },
                error: function () {
                }
            }).always(function () {
                show_other.removeClass('loading');
                ajax_sendding = false;
                if (last_page < page) {
                    $('#btn-bottom').hide();
                }
            });
            return false;
        });
    </script>
@stop
