<?php

namespace Frontend\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Image
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class Image extends Model
{
    /**
     * @var string
     */
    protected $table = 'images';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'alt', 'url'
    ];

    /**
     * Get the posts for the image.
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('Frontend\Model\Post');
    }

    /**
     * Get the release_numbers for the image.
     * @return HasMany
     */
    public function release_numbers()
    {
        return $this->hasMany('Frontend\Model\ReleaseNumber', 'image_id');
    }
}
