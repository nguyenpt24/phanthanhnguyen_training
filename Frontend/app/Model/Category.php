<?php

namespace Frontend\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Category
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'parent_id'
    ];

    /**
     * Get the posts for the image.
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('Frontend\Model\Post', 'category_parent');
    }

    /**
     * Get the release_numbers for the image.
     * @return HasMany
     */
    public function release_numbers()
    {
        return $this->hasMany('Frontend\Model\ReleaseNumber', 'image_id');
    }

    /**
     * Get the child categories.
     * @return HasMany
     */
    public function childes()
    {
        return $this->hasMany('Frontend\Model\Category', 'parent_id');
    }
}
