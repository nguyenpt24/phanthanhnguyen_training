<?php

namespace Frontend\Model;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'mail', 'password', 'role_id', 'gender', 'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the release number that owns the user.
     * @return BelongsToMany
     */
    public function release_numbers_paid()
    {
        return $this->belongsToMany('Frontend\Model\ReleaseNumber', 'payments', 'user_id', 'release_number_id');
    }
}
