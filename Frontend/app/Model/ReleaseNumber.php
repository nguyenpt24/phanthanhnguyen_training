<?php

namespace Frontend\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ReleaseNumber
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumber extends Model
{
    /**
     * @var string
     */
    protected $table = 'release_numbers';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'image_id', 'description', 'price',
    ];

    /**
     * Get the posts for the release number.
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('Frontend\Model\Post');
    }

    /**
     * Get the image that owns the release number.
     * @return BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('Frontend\Model\Image');
    }

    /**
     * Get the image that owns the release number.
     * @return BelongsToMany
     */
    public function users_paid()
    {
        return $this->belongsToMany('Frontend\Model\User', 'payments', 'release_number_id', 'user_id');
    }
}
