<?php

namespace Frontend\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Post
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class Post extends Model
{
    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'image_id', 'release_date', 'content', 'status', 'user_id', 'category_parent', 'category_child', 'release_number_id'
    ];

    /**
     * Get the release number that owns the post.
     * @return BelongsTo
     */
    public function release_number()
    {
        return $this->belongsTo('Frontend\Model\ReleaseNumber', 'release_number_id');
    }

    /**
     * Get the image that owns the release number.
     * @return BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('Frontend\Model\Image');
    }

    /**
     * Get the user that owns the post.
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Frontend\Model\User', 'user_id');
    }

    /**
     * Get the category parent that owns the post.
     * @return BelongsTo
     */
    public function fcategory_parent()
    {
        return $this->belongsTo('Frontend\Model\Category', 'category_parent');
    }

    /**
     * Get the category child that owns the post.
     * @return BelongsTo
     */
    public function fcategory_child()
    {
        return $this->belongsTo('Frontend\Model\Category', 'category_child');
    }
}
