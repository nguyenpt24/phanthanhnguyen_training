<?php

namespace Frontend\Repositories\Eloquent;

use Frontend\Repositories\Contracts\RepositoryInterface;

/**
 * Class CategoryRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Frontend\Model\Category';
    }

    /**
     * Get category menu display
     * @return mixed
     */
    public function getMenuDisplay()
    {
        return $this->_model->with('childes')->select('id', 'name', 'parent_id')->where('menu_display', 1)->limit(7)->get();
    }

    /**
     * Get category menu global display
     * @return mixed
     */
    public function getMenuGlobal()
    {
        return $this->_model->with('childes')->select('id', 'name', 'parent_id')->where('global_display', 1)->limit(7)->get();
    }
}
