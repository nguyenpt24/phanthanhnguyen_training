<?php

namespace Frontend\Repositories\Eloquent;

use Frontend\Repositories\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class ReleaseNumberRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumberRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Frontend\Model\ReleaseNumber';
    }

    /**
     * Get top 5 release number latest
     * @return mixed
     */
    public function getTopFiveLatest()
    {
        $now = date('Y-m-d');
        return $this->_model->select('id', 'name', 'image_id', 'description')
            ->where(DB::raw("STR_TO_DATE(name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->orderBy('id', 'DESC')->limit(5)->get();
    }

    /**
     * Get one or fail
     * @param int $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        $now = date('Y-m-d');
        return $this->_model->select('id', 'name', 'image_id', 'description')
            ->where(DB::raw("STR_TO_DATE(name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->findOrFail($id);
    }
}
