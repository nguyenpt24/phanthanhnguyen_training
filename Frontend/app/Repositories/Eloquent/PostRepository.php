<?php

namespace Frontend\Repositories\Eloquent;

use Frontend\Repositories\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class PostRepository
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostRepository extends EloquentRepository implements RepositoryInterface
{
    /**
     * Specify Model class name
     */
    public function getModel()
    {
        return 'Frontend\Model\Post';
    }

    /**
     * Retrieve all data of repository
     * @return mixed
     */
    public function getAllData()
    {
        return $this->_model->with('image')->get();
    }

    /**
     * Get top 4 posts latest
     * @return mixed
     */
    public function getTopFourLatest()
    {
        $now = date('Y-m-d');
        return $this->_model->select('posts.id', 'posts.title', 'release_date', 'posts.image_id')
            ->join('release_numbers as rn', 'rn.id', 'posts.release_number_id')
            ->where(DB::raw("STR_TO_DATE(rn.name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->where('status', 1)->orderBy('posts.id', 'DESC')->limit(4)->get();
    }

    /**
     * Get posts index limit 8
     * @param int $page
     * @return mixed
     */
    public function getItemsIndex($page = 1)
    {
        $now = date('Y-m-d');
        $per_page = 8;
        $array_top4 = $this->getTopFourLatest()->toArray();
        $ar_id = array();
        foreach ($array_top4 as $item) {
            $ar_id[] = $item['id'];
        }
        return $this->_model->where('status', 1)
            ->join('release_numbers as rn', 'rn.id', 'posts.release_number_id')
            ->where(DB::raw("STR_TO_DATE(rn.name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->orderBy('posts.id', 'DESC')->whereNotIn('posts.id', $ar_id)
            ->paginate($per_page, ['posts.id', 'posts.title', 'release_date', 'posts.image_id', 'posts.content'], 'page', $page);
    }

    /**
     * Get posts
     * @param int $category_id
     * @param int $page
     * @return mixed
     */
    public function getItemsByCategory($category_id, $page = 1)
    {
        $now = date('Y-m-d');
        $per_page = 8;
        return $this->_model->where('status', 1)
            ->join('release_numbers as rn', 'rn.id', 'posts.release_number_id')
            ->where(DB::raw("STR_TO_DATE(rn.name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->where(function ($query) use ($category_id) {
                $query->where('posts.category_parent', $category_id)
                    ->orWhere('posts.category_child', $category_id);
                return $query;
            })
            ->orderBy('posts.id', 'DESC')
            ->paginate($per_page, ['posts.id', 'posts.title', 'release_date', 'posts.image_id', 'posts.content'], 'page', $page);
    }

    /**
     * Get one or fail
     * @param int $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        $now = date('Y-m-d');
        $result = $this->_model->select('posts.id', 'posts.title', 'release_date', 'posts.image_id', 'posts.content', 'posts.user_id', 'posts.release_number_id', 'posts.created_at')
            ->join('release_numbers as rn', 'rn.id', 'posts.release_number_id')
            ->where(DB::raw("STR_TO_DATE(rn.name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->findOrFail($id);
        return $result;
    }

    /**
     * Search Method
     * @param string $content_search
     * @param int $page
     * @return mixed
     */
    public function search($content_search, $page = 1)
    {
        $now = date('Y-m-d');
        $per_page = 8;
        return $this->_model->where('status', 1)
            ->join('release_numbers as rn', 'rn.id', 'posts.release_number_id')
            ->where(DB::raw("STR_TO_DATE(rn.name,'%Y\年%m\月%d\日号')"), '<', $now)
            ->where(function ($query) use ($content_search) {
                $query->where('title', 'LIKE', '%' . $content_search . '%')
                    ->orWhere('content', 'LIKE', '%' . $content_search . '%');
                return $query;
            })
            ->orderBy('posts.id', 'DESC')
            ->paginate($per_page, ['posts.id', 'posts.title', 'release_date', 'posts.image_id', 'posts.content'], 'page', $page);
    }

    /**
     * Get posts by $release_number_id
     * @param int $release_number_id
     * @param int $page
     * @return mixed
     */
    public function getItemsByReleaseNumber($release_number_id, $page = 1)
    {
        $per_page = 8;
        return $this->_model->where('status', 1)
            ->where('release_number_id', $release_number_id)
            ->orderBy('id', 'DESC')
            ->paginate($per_page, ['id', 'title', 'release_date', 'image_id', 'content'], 'page', $page);
    }
}
