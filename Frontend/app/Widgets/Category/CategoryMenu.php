<?php

namespace Frontend\Widgets\Category;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repositories\Eloquent\CategoryRepository;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class CategoryMenu
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryMenu extends AbstractWidget
{
    /**
     * The configuration array.
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @throws BindingResolutionException
     */
    public function run()
    {
        $category_repository = new CategoryRepository();
        $list_items = $category_repository->getMenuDisplay();
        return view('widgets.category.category_menu', [
            'config' => $this->config,
            'list_items' => $list_items,
        ]);
    }
}
