<?php

namespace Frontend\Widgets\Category;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repositories\Eloquent\CategoryRepository;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class CategoryGlobalMenu
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryGlobalMenu extends AbstractWidget
{
    /**
     * The configuration array.
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @throws BindingResolutionException
     */
    public function run()
    {
        $category_repository = new CategoryRepository();
        $list_items = $category_repository->getMenuGlobal();
        return view('widgets.category.category_global_menu', [
            'config' => $this->config,
            'list_items' => $list_items,
        ]);
    }
}
