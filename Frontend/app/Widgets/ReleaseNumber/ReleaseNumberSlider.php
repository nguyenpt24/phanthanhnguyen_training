<?php

namespace Frontend\Widgets\ReleaseNumber;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repositories\Eloquent\ReleaseNumberRepository;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class ReleaseNumberSlider
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class ReleaseNumberSlider extends AbstractWidget
{
    /**
     * The configuration array.
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @throws BindingResolutionException
     */
    public function run()
    {
        $release_number_repository = new ReleaseNumberRepository();
        $list_items = $release_number_repository->getTopFiveLatest();
        return view('widgets.release_number.release_number_slider', [
            'config' => $this->config,
            'list_items' => $list_items,
        ]);
    }
}
