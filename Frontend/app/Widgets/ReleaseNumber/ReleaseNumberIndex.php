<?php

namespace Frontend\Widgets\ReleaseNumber;

use Arrilot\Widgets\AbstractWidget;

class ReleaseNumberIndex extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $list_items = $this->config['list_items'];
        return view('widgets.release_number.release_number_index', [
            'config' => $this->config,
            'list_items' => $list_items,
        ]);
    }
}
