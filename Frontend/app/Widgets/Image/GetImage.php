<?php

namespace Frontend\Widgets\Image;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repositories\Eloquent\ImageRepository;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class GetImage
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class GetImage extends AbstractWidget
{
    /**
     * The configuration array.
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @throws BindingResolutionException
     */
    public function run()
    {
        $size = $this->config['size'];
        $tmp = explode('x', $size);
        $width = $tmp[0];
        $height = $tmp[1];
        if (!empty($this->config['image'])) {
            $image = $this->config['image'];
            $url = $image->url;
            $tmp = explode('/storage/images/', $url);
            $file_name = $tmp[1];
            $image_repository = new ImageRepository();
            $path = str_replace('Frontend', 'Backend', storage_path() . '/app/public/images');
            $url_image = $image_repository->createThumbnail($path, $file_name, $width, $height);
        } else {
            $url_image = config('constants.image_default');
        }
        return view('widgets.image.get_image', [
            'config' => $this->config,
            'url_image' => $url_image
        ]);
    }
}
