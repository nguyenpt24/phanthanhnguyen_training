<?php

namespace Frontend\Widgets\Post;

use Arrilot\Widgets\AbstractWidget;

class Search extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $list_items = $this->config['list_items'];
        return view('widgets.post.search', [
            'config' => $this->config,
            'list_items' => $list_items
        ]);
    }
}
