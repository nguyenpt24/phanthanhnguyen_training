<?php

namespace Frontend\Widgets\Post;

use Arrilot\Widgets\AbstractWidget;
use Frontend\Repositories\Eloquent\PostRepository;
use Illuminate\Contracts\Container\BindingResolutionException;

/**
 * Class PostLatestHome
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostLatestHome extends AbstractWidget
{
    /**
     * The configuration array.
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @throws BindingResolutionException
     */
    public function run()
    {
        $post_repository = new PostRepository();
        $list_items = $post_repository->getTopFourLatest();
        return view('widgets.post.post_latest_home', [
            'config' => $this->config,
            'list_items' => $list_items,
        ]);
    }
}
