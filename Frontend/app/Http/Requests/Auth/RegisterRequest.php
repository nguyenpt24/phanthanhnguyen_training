<?php

namespace Frontend\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $valid_birthday = empty($request->birthday) ? '' : 'date|before:tomorrow';
        return [
            'mail' => 'required|email|max:64|unique:users,mail',
            'password' => 'required|max:72',
            'name' => 'required|max:100',
            'birthday' => $valid_birthday,
        ];
    }
}
