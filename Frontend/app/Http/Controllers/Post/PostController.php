<?php

namespace Frontend\Http\Controllers\Post;

use Frontend\Http\Controllers\Controller;
use Frontend\Repositories\Eloquent\CategoryRepository;
use Frontend\Repositories\Eloquent\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * PostController.php
 * Class PostController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $category_repository;

    /**
     * @var PostRepository
     */
    protected $post_repository;

    /**
     * PostController constructor.
     * @param CategoryRepository $category_repository
     * @param PostRepository $post_repository
     */
    public function __construct(CategoryRepository $category_repository, PostRepository $post_repository)
    {
        $this->category_repository = $category_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Show category dashboard
     * @param int $id
     * @return Factory|View
     */
    public function index($id)
    {
        $post = $this->post_repository->findOrFail($id);
        $author = $post->user ?? null;
        return view('post.index', compact('post', 'author'));
    }

    /**
     * Show category dashboard
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        $search_content = strip_tags($request->s);
        if (isset($search_content) && trim($search_content) != '') {
            $list_posts = $this->post_repository->search($search_content);
            return view('post.search', compact('list_posts'));
        } else {
            return redirect()->route('home.index');
        }
    }
}
