<?php

namespace Frontend\Http\Controllers\Ajax;

use Frontend\Http\Requests\Auth\LoginRequest;
use Frontend\Http\Controllers\Controller;
use Frontend\Http\Requests\Auth\RegisterRequest;
use Frontend\Model\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Auth\Events\Registered;
use Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-18
 */
class AuthController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Check validate login
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = array(
            'mail' => $request->mail,
            'password' => $request->password
        );
        if (Auth::guard()->attempt($credentials)) {
            return response()->json([
                'data' => true,
                'error' => [
                    'status' => false,
                ]
            ]);
        } else {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => __('auth.login.login_fail'),
                ]
            ]);
        }
    }

    /**
     * Check validate register
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        try {
            event(new Registered($user = $this->create($request->all())));
            $this->guard()->login($user);
            return response()->json([
                'data' => true,
                'error' => [
                    'status' => true,
                    'message' => __('auth.login.re_fail'),
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'data' => false,
                'error' => [
                    'status' => true,
                    'message' => __('auth.login.re_fail'),
                ]
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'mail' => $data['mail'],
            'role_id' => config('role.member.id'),
            'password' => Hash::make($data['password']),
            'birthday' => date('Y-m-d', strtotime($data['birthday'])),
            'gender' => $data['gender'],
        ]);
    }
}
