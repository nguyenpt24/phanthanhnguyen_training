<?php

namespace Frontend\Http\Controllers\Ajax;

use Frontend\Repositories\Eloquent\PostRepository;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Frontend\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class PostController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class PostController extends Controller
{
    /**
     * Home index
     * @param Request $request
     * @return Factory|View
     * @throws BindingResolutionException
     */
    public function homeIndex(Request $request)
    {
        $page = $request->page;
        $post_repository = new PostRepository();
        $list_items = $post_repository->getItemsIndex($page);
        return view('ajax.home_post', compact('list_items'));
    }

    /**
     * Ajax category index
     * @param Request $request
     * @return Factory|View
     * @throws BindingResolutionException
     */
    public function categoryIndex(Request $request)
    {
        $page = $request->page;
        $category_id = $request->category_id;
        $post_repository = new PostRepository();
        $list_items = $post_repository->getItemsByCategory($category_id, $page);
        return view('ajax.home_post', compact('list_items'));
    }

    /**
     * Ajax post search
     * @param Request $request
     * @return Factory|View
     * @throws BindingResolutionException
     */
    public function search(Request $request)
    {
        $page = $request->page;
        $search_content = $request->search_content;
        $post_repository = new PostRepository();
        $list_items = $post_repository->search($search_content, $page);
        return view('ajax.home_post', compact('list_items'));
    }

    /**
     * Ajax category index
     * @param Request $request
     * @return Factory|View
     * @throws BindingResolutionException
     */
    public function releaseNumberIndex(Request $request)
    {
        $page = $request->page;
        $release_number_id = $request->release_number_id;
        $post_repository = new PostRepository();
        $list_items = $post_repository->getItemsByReleaseNumber($release_number_id, $page);
        return view('ajax.home_post', compact('list_items'));
    }
}
