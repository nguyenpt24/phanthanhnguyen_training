<?php

namespace Frontend\Http\Controllers;

use Frontend\Repositories\Eloquent\PostRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class HomeController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-25
 */
class HomeController extends Controller
{
    /**
     * @var PostRepository
     */
    protected $post_repository;

    /**
     * Create a new controller instance.
     * @param PostRepository $post_repository
     * @return void
     */
    public function __construct(PostRepository $post_repository)
    {
        $this->post_repository = $post_repository;
    }

    /**
     * Show the application dashboard.
     * @return Factory|View
     */
    public function index()
    {
        $list_posts = $this->post_repository->getItemsIndex();
        return view('home', compact('list_posts'));
    }
}
