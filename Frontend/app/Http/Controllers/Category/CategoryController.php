<?php

namespace Frontend\Http\Controllers\Category;

use Frontend\Repositories\Eloquent\CategoryRepository;
use Frontend\Repositories\Eloquent\PostRepository;
use Frontend\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class CategoryController
 * @author NguyenPT
 * @package rikkeisoft.com
 * @date 2019-06-17
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $category_repository;

    /**
     * @var PostRepository
     */
    protected $post_repository;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $category_repository
     * @param PostRepository $post_repository
     */
    public function __construct(CategoryRepository $category_repository, PostRepository $post_repository)
    {
        $this->category_repository = $category_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Show category dashboard
     * @param int $id
     * @return Factory|View
     */
    public function index($id)
    {
        $category = $this->category_repository->findOrFail($id);
        $list_posts = $this->post_repository->getItemsByCategory($id);
        return view('category.index', compact('category', 'list_posts'));
    }
}
