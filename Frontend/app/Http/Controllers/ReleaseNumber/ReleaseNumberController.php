<?php

namespace Frontend\Http\Controllers\ReleaseNumber;

use Frontend\Repositories\Eloquent\PostRepository;
use Frontend\Repositories\Eloquent\ReleaseNumberRepository;
use Frontend\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class ReleaseNumberController extends Controller
{
    /**
     * @var ReleaseNumberRepository
     */
    protected $release_number_repository;

    /**
     * @var PostRepository
     */
    protected $post_repository;

    /**
     * ReleaseNumberController constructor.
     * @param ReleaseNumberRepository $release_number_repository
     * @param PostRepository $post_repository
     */
    public function __construct(ReleaseNumberRepository $release_number_repository, PostRepository $post_repository)
    {
        $this->release_number_repository = $release_number_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Show release number dashboard
     * @param int $id
     * @return Factory|View
     */
    public function index($id)
    {
        $release_number = $this->release_number_repository->findOrFail($id);
        $list_posts = $this->post_repository->getItemsByReleaseNumber($id);
        return view('release_number.index', compact('release_number', 'list_posts'));
    }
}
